#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE SubsetInt

#include <omp.h>
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "../include/util/SubsetInt.hpp"

using namespace cats;


BOOST_AUTO_TEST_CASE(TestSubsetInt) {
    SubsetInt s;
    BOOST_CHECK_EQUAL(s.contains(0), false);
    s.add(0);
    BOOST_CHECK_EQUAL(s.contains(0), true);
    s.remove(0);
    BOOST_CHECK_EQUAL(s.contains(0), false);

    //* is s_prime in s *//
    SubsetInt s_prime;
    // empty - other
    BOOST_CHECK_EQUAL(s.include(s_prime), true);  // empty - empty
    s.add(0);
    BOOST_CHECK_EQUAL(s.include(s_prime), true);  // empty - singleton
    s.add(66);
    BOOST_CHECK_EQUAL(s.include(s_prime), true);  // empty - several (different size)
    s.remove(66);
    s.remove(0);
    // other - empty
    s_prime.add(0);
    BOOST_CHECK_EQUAL(s.include(s_prime), false);
    s_prime.add(66);
    BOOST_CHECK_EQUAL(s.include(s_prime), false);
    s_prime.remove(66);
    s_prime.remove(0);
    //** several - several
    // size(s_prime) < size(s)
      // empty intersection
    s.add(66);
    s_prime.add(33);
    BOOST_CHECK_EQUAL(s.include(s_prime), false);
      // non-null intersection
    s_prime.add(0);
    s.add(0);
    BOOST_CHECK_EQUAL(s.include(s_prime), false);
      // intersection equal s_prime  s_prime = [0,33] ; s = [0,33][66]
    s.add(33);
    BOOST_CHECK_EQUAL(s.include(s_prime), true);
    BOOST_CHECK_EQUAL(s_prime.include(s), false);

    // size(s_prime) = size(s)
    s_prime.add(99);
      // non-null intersection s_prime = [0,33][99] ; s = [0,33][66]
    BOOST_CHECK_EQUAL(s.include(s_prime), false);
      // intersection equal s_prime  s_prime = [0,33][99] ; s = [0,33][66,99]
    s.add(99);
    BOOST_CHECK_EQUAL(s.include(s_prime), true);
    BOOST_CHECK_EQUAL(s_prime.include(s), false);
      // s_prime = s
    s_prime.add(66);
    BOOST_CHECK_EQUAL(s.include(s_prime), true);
    BOOST_CHECK_EQUAL(s_prime.include(s), true);
}
