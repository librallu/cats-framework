#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE InverseBS

#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/test/included/unit_test.hpp>

#include "InverseGuideBS.hpp"
#include "../include/util/includeAlgorithms.hpp"
#include "../include/SearchManager.hpp"

using namespace cats;


/************* TEST INVERSE BRANCHING SCHEME  ********************/

BOOST_AUTO_TEST_CASE(TestDFS) {
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    DFS dfs = DFS(ts_params);
    dfs.run(1.);
    search_manager.printStats();
    std::cout << "END DFS SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 8);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 0);
    BOOST_CHECK_EQUAL(search_manager.getNbGenerated(), 14);
}

BOOST_AUTO_TEST_CASE(TestBeamSearch1) {
    std::cout << "###### START BS1 SEARCH\n";
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    BeamSearch ts = BeamSearch(ts_params, 1);
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END BS1 SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 1);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 7);
    BOOST_CHECK_EQUAL(search_manager.getNbGenerated(), 6);
}

BOOST_AUTO_TEST_CASE(TestBeamSearch10) {
    std::cout << "###### START BS10 SEARCH\n";
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    BeamSearch ts = BeamSearch(ts_params, 10);
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END BS10 SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 8);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 0);
    BOOST_CHECK_EQUAL(search_manager.getNbGenerated(), 14);
}

BOOST_AUTO_TEST_CASE(TestGreedy) {
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    Greedy ts = Greedy(ts_params);
    // std::cout << "START SEARCH" << std::endl;
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END Greedy SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 1);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 6);
}

BOOST_AUTO_TEST_CASE(TestBranchAndGreedUB) {
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    BranchAndGreed ts = BranchAndGreed(ts_params, 1.);
    std::cout << "START SEARCH\n";
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END Branch and Greed only UB SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 2);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 0);
}

BOOST_AUTO_TEST_CASE(TestBranchAndGreedLB) {
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    BranchAndGreed ts = BranchAndGreed(ts_params, 0.1);
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END Branch and Greed only LB SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 1);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 2);
}

BOOST_AUTO_TEST_CASE(TestLDS0) {
    NodePtr root_ptr = NodePtr(new InverseGuideBS(3));
    SearchManager search_manager = SearchManager();
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    LDS ts = LDS(ts_params);
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END TestLDS 0 SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 7);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 0);
}

BOOST_AUTO_TEST_CASE(TestBeamCombinator) {
    SearchManager search_manager = SearchManager();
    NodePtr root_ptr = NodePtr(new BeamCombinator<>(NodePtr(new InverseGuideBS(3)), search_manager, 1));
    std::cout << "\n\nBeamCombinator Test D=1\n";
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    Greedy ts = Greedy(ts_params);
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END BeamCombinator SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 1);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 6);
}

BOOST_AUTO_TEST_CASE(TestBeamCombinator10) {
    SearchManager search_manager = SearchManager();
    NodePtr root_ptr = NodePtr(new BeamCombinator<>(NodePtr(new InverseGuideBS(3)), search_manager, 10));
    std::cout << "\n\nBeamCombinator Test D=10\n";
    search_manager.start();
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    Greedy ts = Greedy(ts_params);
    ts.run(1.);
    search_manager.printStats();
    std::cout << "END BeamCombinator SEARCH\n";
    NodePtr best_ptr = search_manager.getBest();
    int nb_sol = search_manager.getNbSol();
    BOOST_CHECK_EQUAL(nb_sol, 1);
    BOOST_CHECK_EQUAL(best_ptr.get()->evaluate(), 0);
}