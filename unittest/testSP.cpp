#include "SPInstance.hpp"
#include "SPNode.hpp"
#include "../include/util/includeAlgorithms.hpp"
#include "../include/SearchManager.hpp"

using namespace cats;

/**
 * Computes optimal solution for the Shortest Path problem
 */
double getOptimalSolution(SPInstance& inst) {
    double time_limit = 100.;
    SearchManager search_manager = SearchManager();
    search_manager.set_verbose(false);
    GenericPEStoreHash<PrefixEquivalenceSP, nodeEqHash, DominanceInfos2> dominance_store;
    NodePtr root_ptr = NodePtr(new CatsNodeSP(inst, false));
    root_ptr = NodePtr(new DominanceCombinator2<PrefixEquivalenceSP>(root_ptr, search_manager, dominance_store));
    
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    search_manager.start();
    auto ts = AStar(ts_params);

    ts.run(time_limit);
    return search_manager.getPrimal();
}

int main(int argc, char* argv[]) {

    // compute the instance and optimal solution value
    SPInstance inst(7, 100, .75);
    double optimal = getOptimalSolution(inst);
    std::cout << optimal << std::endl;
    // std::cout << inst.getGraphviz() << std::endl;

    // run tree search
    double time_limit = 100.;
    SearchManager search_manager = SearchManager();
    GenericPEStoreHash<PrefixEquivalenceSP, nodeEqHash, DominanceInfos2> dominance_store;
    NodePtr root_ptr = NodePtr(new CatsNodeSP(inst));
    root_ptr = NodePtr(new StatsCombinator<>(root_ptr, search_manager, search_manager.getSearchStats()));    
    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    search_manager.start();
    // auto ts = DFS(ts_params);
    auto ts = IterativeBeamSearch(ts_params, 1., 2.);

    ts.run(time_limit);

    search_manager.printStats();
    dominance_store.printStats();

    return 0;
}