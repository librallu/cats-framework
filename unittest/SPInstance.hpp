#pragma once

#include <vector>
#include <cassert>
#include <string>
#include <iostream>
#include <map>

#include "../include/numeric.hpp"
#include "../include/io.hpp"

using namespace cats;

typedef uint64_t SPNodeId;


struct SPArc {
    SPNodeId v;
    double c;
};

/**
 * encodes a node informations
 */
struct SPNode {
    std::vector<SPArc> children;
};


/**
 * Models a Shortest path in a acyclic directed graph.
 * Used to test Reinforcement Learning learning models
 */
class SPInstance {

 protected:
    int w_;
    int d_;
    double p_;
    double sp_;
    std::vector<SPNode> nodes_;
    // std::map<std::pair<SPNodeId, SPNodeId>, double> arc_weights_;

 public:
    /**
     * \param w digraph width
     * \param d digraph depth (nb of "hidden" layers)
     * \param p probability to create an edge
     */
    SPInstance(int w, int d, double p) : w_(w), d_(d), p_(p) {
        assert(p >= 0);
        assert(p <= 1);
        createDigraph_();
        computeShortestPath_();
    }

    std::vector<SPArc>& getChildren(SPNodeId n) { return nodes_[n].children; }

    std::string getGraphviz() const {
        std::string res = "digraph {\n";
        for ( int i = 0 ; i < static_cast<int>(nodes_.size()) ; i++ ) {
            for ( SPArc j : nodes_[i].children ) {
                res += "\t" + std::to_string(i) + " -> " + std::to_string(j.v) + "[label=\""+getDoubleAtPrecision(j.c, 2)+"\"];\n";
            }
        }
        res += "}";
        return res;
    }

    bool isGoal(SPNodeId i) const { 
        return i == nodes_.size()-1;
    }

    // double getCost(SPNodeId i, SPNodeId j) {
    //     return arc_weights_[std::make_pair(i,j)];
    // }

 private:
    void createDigraph_() {
        // create root
        nodes_.push_back(SPNode());
        std::vector<SPNodeId> prev_level(1, 0);
        // create intermediate nodes
        for ( int d = 0 ; d < d_ ; d++ ) {
            std::vector<SPNodeId> next_level;
            for ( int w = 0 ; w < w_ ; w++ ) {
                nodes_.push_back(SPNode());
                next_level.push_back(nodes_.size()-1);
            }
            // create arcs between previous level and next level
            populateLevelArcs_(prev_level, next_level);
            // exchange prev_level by next_level for next iteration
            prev_level.swap(next_level);
        }
        // create goal node
        nodes_.push_back(SPNode());
        std::vector<SPNodeId> final_layer(1, nodes_.size()-1);
        populateLevelArcs_(prev_level, final_layer);
        
    }

    void populateLevelArcs_(std::vector<SPNodeId> prev_level, std::vector<SPNodeId> next_level) {
        for ( auto a : prev_level ) {
            for ( auto b : next_level ) {
                if ( randomFloat() < p_ ) {
                    double r = randomInt(0,100);
                    // arc_weights_[std::make_pair(a, b)] = r;
                    nodes_[a].children.push_back({.v = b, .c = r});
                }
            }
        }
    }

    void computeShortestPath_() {

    }
};
