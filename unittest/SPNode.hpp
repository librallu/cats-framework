#pragma once
#include <string>
#include <iostream>
#include <cassert>
#include <unordered_map>
#include <boost/functional/hash.hpp>

#include "../../cats-framework/include/Node.hpp"
#include "../../cats-framework/include/util/SubsetInt.hpp"

#include "SPInstance.hpp"

using namespace cats;

/**
 * SP prefixEquivalence
 */
struct PrefixEquivalenceSP {
    SPNodeId n_;

    PrefixEquivalenceSP(const PrefixEquivalenceSP& n) : n_(n.n_) {}
    PrefixEquivalenceSP(const SPNodeId n) : n_(n) {}

    bool operator==(const PrefixEquivalenceSP& a) const {
        return n_ == a.n_;
    }
};

/**
 * hash function taking a prefixEquivalence as a parameter
 */
struct nodeEqHash {
    size_t operator()(const PrefixEquivalenceSP& n) const noexcept {
        return n.n_;
    }
};


class CatsNodeSP : public PrefixEquivalenceNode<PrefixEquivalenceSP> {
 private:
    SPInstance& inst_;
    std::vector<SPNodeId> prefix_;
    double cost_;
    bool disable_guide_;

 public:
    explicit CatsNodeSP(SPInstance& inst, bool disable_guide = true): Node(), inst_(inst), cost_(0), disable_guide_(disable_guide) {
        prefix_.push_back(0);
    }

    explicit CatsNodeSP(const CatsNodeSP& s): Node(s),
        inst_(s.inst_), prefix_(s.prefix_), cost_(s.cost_), disable_guide_(s.disable_guide_) {}

    inline NodePtr copy() const override { return NodePtr(new CatsNodeSP(*this)); }

    inline double evalPrefix() const override {
        if ( isGoal() ) return cost_;
        return disable_guide_ ? 0 : cost_;
    }

    inline std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for ( auto e : inst_.getChildren(getLastVertex()) ) {
            CatsNodeSP* child = new CatsNodeSP(*this);
            child->addVertex(e);
            res.push_back(NodePtr(child));
        }
        return res;
    }

    inline bool isGoal() const override {
        return(inst_.isGoal(getLastVertex()));
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {}

    inline PrefixEquivalenceSP getPrefixEquivalence() const override {
        return PrefixEquivalenceSP(getLastVertex());
    }

    std::string getName() override {
        return "SP";
    }

 private:

    SPNodeId getLastVertex() const {
        return prefix_[prefix_.size()-1];
    }

    /**
     * \brief adds a given vertex to the prefix
     */
    inline void addVertex(SPArc a) {
        prefix_.push_back(a.v);
        cost_ += a.c;
    }
};
