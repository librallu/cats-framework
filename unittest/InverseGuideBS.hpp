#pragma once

#include <cmath>

#include "../include/Node.hpp"

using namespace cats;

/**
 * \class InverseGuideBS
 * \brief dummy branching scheme where guide function contradicts the evaluation function
 * A node is defined by its eval function, and guide (eval, guide)
 *                    0,0    
 *         /                     \
 *        0,0                     0,-4
 *    /        \                /      \
 *   0,0       0,-2          0,-4       0,-6
 *  /    \     /    \        /  \      /    \
 * 0,0  1,-1  2,-2  3,-3   4,-4 5,-5  6,-6  7,-7
 */
class InverseGuideBS : public Node {
 private:
    int v_;
    int depth_;

 public:
    explicit InverseGuideBS(int depth) : Node(), v_(0), depth_(depth) {
        std::cout << "OPEN1 NODE:\t" << v_ << "\t" << depth_ << std::endl;
    }

    InverseGuideBS(const InverseGuideBS& n, int depth, int v) : Node(n), v_(v), depth_(depth) {
        std::cout << "OPEN2 NODE:\t" << v_ << "\t" << depth_ << std::endl;
    }

    InverseGuideBS(const InverseGuideBS& n) : Node(n), v_(n.v_), depth_(n.depth_) {
        std::cout << "OPENcopy NODE:\t" << v_ << "\t" << depth_ << std::endl;
    }

    double evaluate() const override {
        return depth_ == 0 ? v_ : -v_;
    }

    double guide() override {
        return -v_;
    }

    bool isGoal() const override {
        return depth_ == 0;
    }

    std::string getName() override {
        return "Inv";
    }

    NodePtr copy() const override {
        InverseGuideBS* tmp = new InverseGuideBS(*this);
        return NodePtr(tmp);
    }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        if ( depth_ > 0 ) {
            res.push_back(NodePtr(new InverseGuideBS(*this, depth_ - 1, v_)));
            res.push_back(NodePtr(new InverseGuideBS(*this, depth_ - 1, v_ + pow(2, depth_-1) )));
        }
        return res;
    }
};
