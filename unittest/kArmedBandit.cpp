#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

#include "../include/util/RL/UCB.hpp"
#include "../include/util/RL/Egreedy.hpp"
#include "../include/util/RL/GradientBandit.hpp"
#include "../include/numeric.hpp"
#include "../include/SearchStats.hpp"

using namespace cats;
using namespace rl;


/**
 * \brief Test benchmark for a bandit algorithm. Generates various slots with
 * average rewards uniformly on [-1;1]. Each arm generates a reward using a Normal
 * generator with average deviation of 1
 */
class KArmedBandits {
 protected:
    std::vector<double> slots_;
    std::vector<std::normal_distribution<double>> distribs_;
    std::default_random_engine& generator_;
    int optimal_;

 public:
    /**
     * \param n slot number
     * Builds n slots biased with 
     */
    KArmedBandits(int n, std::default_random_engine& generator) : generator_(generator) {
        for ( int i = 0 ; i < n ; i++ ) {
            slots_.push_back(randomFloat(-1, 1));
            distribs_.push_back(std::normal_distribution<double>(slots_[i], 1.));
        }
        computeOptimalChoice();
    }

    double pull(int n) {
        return distribs_[n](generator_);
    }

    int getOptimalChoice() {
        return optimal_;
    }

 protected:

    inline void computeOptimalChoice() {
        optimal_ = 0;
        for ( int i = 1 ; i < static_cast<int>(slots_.size()) ; i++ ) {
            if ( slots_[i] > slots_[optimal_] ) {
                optimal_ = i;
            }
        }
    }

};


/**
 * \brief Benchmarks a Bandit algorithm and outputs convergence curves
 * \param rl Bandit algorithm to benchmark
 * \param freward output json to print reward evolution
 * \param fopt output json 
 * \param N nb experiments
 * \param M nb timesteps
 * \param K nb slots
 */
void banditBenchmark(BanditAlgorithm& rl, std::string freward, std::string fopt,
std::string s, int N = 2000, int M = 1000, int K = 10) {
    std::default_random_engine generator;
    std::vector<double> rewards(M, 0.);
    std::vector<int> nbopt(M, 0);

    // run simulation loop
    for ( int i = 0 ; i < N ; i++ ) {
        KArmedBandits env(K, generator);  // define new environment at each iteration
        rl.reinitialize();
        for ( int j = 0 ; j < M ; j++ ) {
            int a = rl.choice();
            double r = env.pull(a);
            rl.update(a, r);
            rewards[j] += r;
            if ( a == env.getOptimalChoice() ) {
                nbopt[j]++;
            }
        }
    }
    
    // write convergence curves
    AnytimeCurve curve_reward;
    AnytimeCurve curve_opt;
    for ( int i = 0 ; i < M ; i++ ) {
        curve_reward.addPoint({static_cast<double>(i), 0, 0, 0, 0, 0, 0, rewards[i]/static_cast<double>(N)});
        curve_opt.addPoint({static_cast<double>(i), 0, 0, 0, 0, 0, 0, nbopt[i]/static_cast<double>(N)});
    }

    // export curves
    std::ofstream f;

    f.open(freward);
    f << curve_reward.toJson(s+" reward");
    f.close();

    f.open(fopt);
    f << curve_opt.toJson(s+" opt");
    f.close();
}


int main(int argc, char* argv[]) {
    int N = 2000;
    int M = 4000;
    int K = 10;
    

    // double c = 1.;
    // std::string sc = std::to_string(static_cast<int>(c));
    // std::string s = "UCB("+sc+")";
    // UCB ucb(K, c);
    // banditBenchmark(ucb, "experiments/ucb_"+sc+"_r.json", "experiments/ucb_"+sc+"_o.json", s, N, M, K);

    // double e = 0.0;
    // std::string sc = std::to_string(e);
    // std::string s = "egreedy("+sc+")";
    // Egreedy egreedy(K, e, 5);
    // banditBenchmark(egreedy, "experiments/egreedy_"+sc+"_r.json", "experiments/egreedy_"+sc+"_o.json", s, N, M, K);

    double a = 0.1;
    bool b = true;
    std::string sc = std::to_string(a)+(b?"_baseline":"");
    std::string s = "grad("+sc+")";
    GradientBandit grad(K, a, b);
    banditBenchmark(grad, "experiments/grad_"+sc+"_r.json", "experiments/grad_"+sc+"_o.json", s, N, M, K);

    return 0;
}