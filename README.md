# CATS-Framework (Combinator-based Anytime Tree Search)

Implements several state-of-the-art Tree Search algorithms in a generic way. It allows to easily solve hard combinatorial optimization problems.

**authors:** Abdel-Malik Bouhassoun, Luc Libralesso


## usage

**WARNING:** unit tests are currently out of date. They will be corrected soon

 - compilation: `cmake . ; make`
 - compilation and run unit tests: `cmake . ; make ; make test`


## generate documentation

CATS uses doxygen to generate documentation

 - documentation generation: `doxygen`

This command will generate in the folder *doc/html* the HTML version of the documentation. One can open *doc/html/index.html* with a web browser.


## common errors


### Some boost libraries are missing

On a linux (debian-like) system you can install them with the following command: ```sudo aptitude install libboost-all-dev```




## TODO

- [x] remove old .cpp files
- [x] refactor compilation process
- [x] remove openMP dependancy
- [ ] Add search metrics: fathomed/trashed/dumped
- [ ] Add PriorityQueue Interface
- [ ] refactor unit tests
- [ ] add const + mutables in framework
- [ ] SearchManager interface (use a StandardSearchManager)
- [ ] improve RouletteWheelGreedy algorithm performance (by dichotomic search)



 ## Credits

- logo adapted from [here](https://all-free-download.com/free-vector/download/different-cats-vector-illustration_529485.html)