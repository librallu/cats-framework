#pragma once

#include "Store.hpp"


namespace cats {

/**
 * \brief stores informations associated to a SolutionPart learned during the search, including the pheromones.
 */
struct SolutionPartInfos {
    double pheromones;
    double average;
    uint64_t nb_sols;
};

/**
 * \brief stores 
 */
template <typename SolPart>
class SolutionPartsStore : public Store<SolPart, SolutionPartInfos> {
 public:
    bool contains(const SolPart& sp) const override = 0;  ///< returns true if store contains *sp*
    SolutionPartInfos get(const SolPart& sp) const override = 0;  ///< returns value linked to *sp*
    void set(const SolPart& sp, const SolutionPartInfos& v) override = 0;  ///< registers the value *v* to be linked to *sp*
};

}  // namespace cats
