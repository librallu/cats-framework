#pragma once

#include <vector>
#include <memory>
#include <string>
#include <iostream>
#include <functional>
#include <algorithm>
#include <fstream>

#include "io.hpp"
#include "Store.hpp"

namespace cats {

/** \brief exception indicating that a nextChild is called on empty remaining children
 */
struct NoMoreChildrenException : public std::exception {
    const char* what() const throw() {
        return "NoMoreChildrenException: no more children (maybe you forgot to call hasChildren() before ?)";
    }
};


class Node;   // forward definition of Node Class

/**
 * \brief shortcut definition for Pointer on Node
 */
typedef std::shared_ptr<Node> NodePtr;


/** \class Node Node.hpp
 *  \brief Defines an implicit search tree node. Used as an interface between the optimization problem specific parts and the generic search algorithms.
 * 
 * This class implements the implicit search tree. Constructors represent the definition of the root node. get[Lazy]Children
 * represent the children of this given node. All informations above encode the Implicit Search Tree. 
 * It can provide additional informations
 * as evaluate() which is a lower bound (objective value if feasible solution). isGoal() tells if the solution is feasible *etc.*
 */
class Node {
 protected:
    bool lc_;   ///< (lazy children) set to true if lazy children were used
    bool children_initialized_;   ///< children list initialized within a node
    std::vector<NodePtr> remaining_children_;  ///< list of remaining children in lazy children interface

 public:
    explicit Node() : lc_(false), children_initialized_(false) {}

    Node(const Node& n) : lc_(false), children_initialized_(false) {}

    /**
     * \brief Returns the children of a given node
     */
    virtual std::vector<NodePtr> getChildren() {
        if ( lc_ ) {
            std::cout << "getChildren not implemented" << std::endl;
            throw new NotImplementedException();
        } else {
            // try to use lazy children generation if available
            lc_ = true;
            std::vector<NodePtr> res;
            while ( hasNextChild() ) {
                res.push_back(nextChild());
            }
            return res; 
        }
    }

    /**
     * \brief Lazy children generation: returns the next child. Possibly updates the evaluate() value
     */
    virtual NodePtr nextChild() {
        if ( !children_initialized_ ) {
            remaining_children_ = getChildren();
            std::reverse(remaining_children_.begin(), remaining_children_.end());
            children_initialized_ = true;
        }
        if ( remaining_children_.empty() ) {
            throw new NoMoreChildrenException();
        }
        NodePtr elt = remaining_children_[remaining_children_.size()-1];
        remaining_children_.pop_back();
        return elt;
    }

    /**
     * \brief returns true if the node has an additional child
     */
    virtual bool hasNextChild() {
        if ( !children_initialized_ ) {
            remaining_children_ = getChildren();
            std::reverse(remaining_children_.begin(), remaining_children_.end());
            children_initialized_ = true;
        }
        return !remaining_children_.empty();
        // throw new NotImplementedException();
    }

    /**
     * \brief returns the prefix g(x) of a given node (cost of the choices made so far)
     * A lower bound is defined as f(x) = g(x)+h(x)
     */
    virtual double evalPrefix() const {
        return 0;
    }

    /**
     * \brief returns the suffix h(x) of a given node. It is an optimistic estimate of the remaining cost 
     */
    virtual double evalSuffix() const {
        return 0;
    }

    /** \brief evaluation of the node. dual bound if not goal, and objective value if goal
     * \param[out] evaluation of the node
     */
    virtual double evaluate() const {
        return evalPrefix()+evalSuffix();
    }

    /** \brief guide of the node. Indicates the quality estimate of this node
     * By default corresponds to the dual bound
     */
    virtual double guide() {
        return evaluate();
    }

    /**
     * \brief returns true iff current node is a goal node
     */
    virtual bool isGoal() const = 0;

    /**
     * \brief returns a NodePtr containing a copy of the node
     */
    virtual NodePtr copy() const = 0;

    /**
     * \brief returns a solution converted to string
     */
    virtual std::string solToStr() {return "";}

    /**
     * \brief writes a solution in a file if it exists
     */
    virtual void writeSol(std::string filename){
        std::ofstream f;
        f.open(filename);
        if ( !f.is_open() ) throw FileNotFoundException();
        f << solToStr() << "\n";
        f.close();
    }

    /**
     *  Is called when the tree search algorithm finds a new best solution.
     */
    virtual void handleNewBest() {
    }

    /**
     * Called at the end of the search. The Problem specific Node implementation
     * can use it to execute actions after the search ends.
     */
    virtual void handleEndSearch() {}

    /**
     * \brief returns name of the Implicit Search Tree
     */
    virtual std::string getName() {
        return "[TODO override getName]";
    }

    /**
     * \brief sets tree search name. It can be useful to report the name of the tree search
     * in case of probing within the branching scheme.
     */
    virtual void setTreeSearchName(std::string* alg_name) {}

    /**
     * \brief sets if the branching scheme should perform cuts or not
     */
    virtual void set_enable_cuts(bool v) {}

    /**
     * \brief returns node completion percentage
     */
    virtual double nodeCompletion() {
        std::cout << "Node.nodeCompletion" << std::endl;
        throw new NotImplementedException();
    }

    /**
     * \brief returns true if node has a parent, false if not
     */
    virtual bool hasParent() const {
        std::cout << "Node.hasParent" << std::endl;
        throw new NotImplementedException();
    }

    /**
     * \brief returns the parent if the node has some
     */
    virtual NodePtr getParent() const {
        std::cout << "Node.getParent" << std::endl;
        throw new NotImplementedException();
    }

    /**
     * \brief return the list of ancestors. First element is the node itself and the last element is the root.
     */
    virtual std::vector<NodePtr> getAllParents() const {
        std::vector<NodePtr> res;
        res.push_back(copy());
        while ( res[res.size()-1].get()->hasParent() ) {
            res.push_back(res[res.size()-1].get()->getParent());
        }
        return res;
    }

    /**
     * \brief used to deconstruct combinators. Used to provide the original node without combinators
     */
    virtual NodePtr getOriginalBS() {
        throw new NotImplementedException();
    }

    /**
     * used to provide pareto fronts of the nodes (used for pareto dominances)
     */
    virtual std::vector<int32_t> getParetoFronts() {
        throw new NotImplementedException();
    }

    /**
     * \brief provides information about the node
     */
    virtual std::string nodemsg() { return ""; }

    /**
     * \brief provides information about the last decision taken
     */
    virtual std::string arcmsg() { return ""; }

    /**
     * \brief registers a new tree search iteration.
     * Used for DominanceCombinator2 to fathom equivalent nodes within a same search
     */
    virtual void newIter() {}

};


typedef int SolPartId;

/**
 * \brief It adds an adaptative guide depending on Solutions Parts.
 * It is used to implement Ant Colony Optimization within the tree search framework.
 */
template <typename SolPart>
class SolutionPartsNode : virtual public Node {
 public:
    /**
     * \brief returns last solutions parts. In classical ACO, it should be the last edge selected.
     * In some variants, one can want to mix different pheromone structures within one algorithm.
     * The vector return value allows that.
     */
    virtual std::vector<SolPart> getLastSolutionPart() = 0;

    /**
     * \brief returns all solutions parts used by the partial solution.
     * Used to update pheromones values at the end of an ACO iteration.
     */
    virtual std::vector<SolPart> getAllSolutionParts() = 0;

    /**
     * \brief enumerates all solutions parts to be enumerated while pheromones initialization
     */
    virtual std::vector<SolPart> enumerateSolutionParts() = 0;

    /**
     * \brief Used as a heuristic guide by the Ant Colony Optimization. By default, uses the branching scheme guide
     * Can be overwritten in order to have a different guide for ant colony optimization and non-adaptative tree searches.
     */
    virtual double guideSP() { return guide(); }

    /**
     * \brief returns the id of a given solution part
     */
    virtual SolPartId getSolPartId(SolPart sp) { return 0.; }

    /**
     * \brief returns the weight of a given solution part. Can be used to make different pheromones structures
     * weighted differently to make one more important.
     */
    virtual int getSolutionPartWeight(SolPartId id) { return 1; }

    /**
     * \brief returns the evaporation rate of a given solution part.
     */
    virtual double getSolutionPartEvaporationRate(SolPartId id) { return 0.1; }
};


/** Defines an additional interface allowing to get the equivalence class of a given node.
 */
template <typename T>
class PrefixEquivalenceNode : virtual public Node {
 public:
    /**
     * \brief returns an prefix equivalence class.
     * Examples: for a TSP, it is a subset of cities and a last city
     */
    virtual T getPrefixEquivalence() const = 0;

    /**
     * \brief returns the cost of the equivalence class. By default, the prefix function
     */
    virtual double PEevalPrefix() const { return evalPrefix(); }
};

}  // namespace cats
