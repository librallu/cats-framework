#pragma once

#include <exception>
#include "io.hpp"

namespace cats {

/**
 * \brief raised when one try to access to an incorrect place in the pheromone store.
 */
class NotFoundException: public std::exception {
  virtual const char* what() const throw() {
    return "not found in pheromone store";
  }
};

/**
 * \brief abstract class to store informations about subsets of nodes
 * Fundamentally, pheromones or memorization techniques can be added on nodes of the search tree.
 * In the case of a TSP like problem, pheromones will be put on the last decision made at the
 * node (i.e. the last edge). Memorization will be a subset of added vertices with a last added vertex.
 * T: Node Partition
 */
template <typename T, typename U = double>
class Store {
 public:
  virtual bool contains(const T& n) const = 0;  ///< returns true if store contains *n*
  virtual U& get(const T& n) = 0;  ///< returns value linked to *n*
  virtual void set(const T& n, const U& v) = 0;  ///< registers the value *v* to be linked to *n*
};


}  // namespace cats
