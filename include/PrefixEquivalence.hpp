#pragma once

#include <iomanip>
#include <set>
#include <mutex>

#include "Store.hpp"

namespace cats {

/**
 * \brief PrefixEquivalenceInfos store informations about the prefix equivalence class
 * Contains:
 * - **prefix_primal:** the best prefix found (used for dominance/symmetry breaking)
 * - **prefix_dual:** the best prefix dual bound found
 * - **suffix_dual:** the best suffix dual bound found
 * - **suffix_primal:** the best suffix primal bound found (solution value minus prefix value)
 */
struct PrefixEquivalenceInfos {
    double prefix_primal;
    double prefix_dual;
    double suffix_dual;
    double suffix_primal;

    friend std::ostream &operator<<(std::ostream& out, const PrefixEquivalenceInfos& n) {
        out << "(" << n.prefix_primal << "," << n.prefix_dual << "|" << n.suffix_dual << "," << n.suffix_primal << ")";
        return out;
    }
};


/**
 * \brief DominanceInfos store informations about dominances
 * Contains:
 * - **prefix_primal:** the best prefix found (used for dominance/symmetry breaking)
 */
struct DominanceInfos {
    double prefix_primal;

    friend std::ostream &operator<<(std::ostream& out, const DominanceInfos& n) {
        out << "(" << n.prefix_primal << ")";
        return out;
    }
};


typedef uint32_t IterNumber;

/**
 * \brief DominanceInfos2 store informations about dominances
 * Contains:
 * - **prefix_primal:** the best prefix found (used for dominance/symmetry breaking)
 * - **iter_number:** 
 */
struct DominanceInfos2 {
    double prefix_primal;
    IterNumber iter_number;

    friend std::ostream &operator<<(std::ostream& out, const DominanceInfos2& n) {
        out << "(" << n.prefix_primal << "," << n.iter_number << ")";
        return out;
    }
};


/**
 * \brief PrefixEquivalenceStore
 * Stores PrefixEquivalenceInfos/DominanceInfos during the search
 */
template <typename T, typename I=PrefixEquivalenceInfos>
class PrefixEquivalenceStore : public Store<T, I> {
 public:
    mutable uint64_t nb_cuts_ = 0;
    mutable uint64_t nb_stores_ = 0;
    mutable uint64_t nb_gets_ = 0;
    mutable uint64_t nb_contains_ = 0;

    bool contains(const T& n) const override = 0;  ///< returns true if store contains *n*
    I& get(const T& n) override = 0;  ///< returns value linked to *n*
    void set(const T& n, const I& v) override = 0;  ///< registers the value *v* to be linked to *n*
    virtual size_t getNbElts() = 0;  ///< size of the store
    virtual std::vector<T> getKeys() = 0;  ///< list of keys present within the store

    /**
     * \brief registers a new cut done by some algorithm by using the PrefixEquivalenceStore
     */
    inline void registerCut() {
        nb_cuts_++;
    }

    /**
     * \brief prints prefix equivalence stats
     */
    inline void printStats() {
        std::cout << title1String("Prefix Equivalence statistics", 40, "=") << std::endl;
        std::cout << "nb elements in store\t" << dotNumber(getNbElts()) << std::endl;
        std::cout << "nb prefix eq cuts\t" << dotNumber(nb_cuts_) << std::endl;
        std::cout << "nb prefix eq stores\t" << dotNumber(nb_stores_) << std::endl;
        std::cout << "nb prefix eq accesses\t" << dotNumber(nb_gets_) << std::endl;
        std::cout << "nb PE exist tests\t" << dotNumber(nb_contains_) << std::endl;
    }

};




/**
 * \brief Implements a generic hash prefix equivalence store
 */
template <typename PE, typename PEhash, typename I=PrefixEquivalenceInfos>
class GenericPEStoreHash : public PrefixEquivalenceStore<PE, I> {
 private:
    std::unordered_map<PE, I, PEhash> content;
    mutable std::mutex lock_;

 public:
    GenericPEStoreHash() {}

    void showContent() {
        std::cout << title1String("PE store content", 75, "=") << std::endl;
        for ( auto e : content ) {
            std::cout << e.first;
            std::cout << "\t";
            std::cout << e.second;
            std::cout << std::endl;
        }
        std::cout << title1String("", 75, "-") << std::endl;
    }

    bool contains(const PE& n) const override {
        lock_.lock();
        auto it = content.find(n);
        bool res = !(it == content.end());
        this->nb_contains_++;
        lock_.unlock();
        return res;
    }

    I& get(const PE& n) override {
        lock_.lock();
        assert(!(content.find(n) == content.end()));
        I& res = content.at(n);
        this->nb_gets_++;
        lock_.unlock();
        return res;
    }

    void set(const PE& n, const I& v) override {
        lock_.lock();
        content[n] = v;
        this->nb_stores_++;
        lock_.unlock();
    }

    size_t getNbElts() override {
        return content.size();
    }

    std::vector<PE> getKeys() override {
        std::vector<PE> res;
        for ( auto e : content ) {
            res.push_back(e.first);
        }
        return res;
    }

};

}  // namespace cats
