#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <cassert>

#include "io.hpp"

namespace cats {

typedef uint64_t NodeNumber;
typedef int32_t Depth;  ///< encodes a depth in a tree


/**
 * \brief encodes a point (multiple x axis like time, nb_expanded, ...) and a value v (solution quality, bound quality, etc.)
 */
struct AnytimePoint {
    double time;
    NodeNumber nb_generated;
    NodeNumber nb_expanded;
    NodeNumber nb_trashed;
    NodeNumber nb_pruned;
    NodeNumber nb_eval;
    NodeNumber nb_guide;
    double v;
};

/**
 * \brief registers a serie of points encoding the evolution of a given value v
 */
class AnytimeCurve {
 private:
    std::vector<AnytimePoint> pts;

 public:
    void addPoint(AnytimePoint p) {
        pts.push_back(p);
    }

    inline bool empty() const { return pts.empty(); }

    AnytimePoint getLast() const { return pts[pts.size()-1]; }

    std::string toJson(std::string title) {
        std::string res = "{\"title\": \""+title+"\", \"points\":[";
        for ( auto i = 0 ; i < static_cast<int>(pts.size()) ; i++ ) {
            AnytimePoint p = pts[i];
            res += "{";
            res += "\"time\":" + std::to_string(p.time) + ",";
            res += "\"nb_generated\":" + std::to_string(p.nb_generated) + ",";
            res += "\"nb_expanded\":" + std::to_string(p.nb_expanded) + ",";
            res += "\"nb_trashed\":" + std::to_string(p.nb_trashed) + ",";
            res += "\"nb_pruned\":" + std::to_string(p.nb_pruned) + ",";
            res += "\"nb_eval\":" + std::to_string(p.nb_eval) + ",";
            res += "\"nb_guide\":" + std::to_string(p.nb_guide) + ",";
            res += "\"v\":" + std::to_string(p.v);
            res += "}";
            if ( i < static_cast<int>(pts.size())-1 ) res += ",";
        }
        res += "]}";
        return res;
    }
};


/**
 * \brief describes a Node opening event during tree search
 */
struct NodeOpeningEvent {
    NodeNumber nodeid;  ///< id of the node being opening
    NodeNumber parent;  ///< parent id (equal to nodeid if root node thus no parent)
    double g;  ///< prefix bound
    double h;  ///< suffix bound
    double guide;  ///< guide
    std::string nodemsg;  ///< message on the node (name, label, additional information, *etc.*)
    std::string arcmsg;  ///< message on the arc (additional information, decision, *etc.*)
    bool goal;  ///< true iff node is goal
};

/**
 * \brief encodes an average value and population size
 */
struct AvgNumber {
    double avg = 0.;
    NodeNumber n = 0;

    /**
     * Updates the average in O(1) in time and memory
     */
    void addValue(double v) {
        n++;
        avg += (1./n)*(v-avg);
    }
};


/**
 * \brief reports search metrics and diverse informations gathered about the search 
 */
class SearchStats {
 private:
    NodeNumber nb_generated_ = 0;  ///< nodes generated (i.e nb of constructor calls)
    NodeNumber nb_expanded_ = 0;  ///< number of nodes expanded (i.e nb of nodes where children were called)
    NodeNumber nb_trashed_ = 0;  ///< number of nodes trashed (i.e, expanded without generating children)
    NodeNumber nb_pruned_ = 0;  ///< number of nodes pruned (i.e, lower bound dominated by best)
    NodeNumber nb_eval_ = 0;  ///< number of node evaluations
    NodeNumber nb_guide_ = 0;  ///< number of guide calls
    AnytimeCurve primal_curve_;  ///< anytime curve of progress
    std::vector<NodeOpeningEvent> node_events_;  ///< node opening events (registers all nodes being opened)
    std::vector<AvgNumber> guidediff_depth_;   ///< guidediff_depth_[d] := average and number of difference of guides between a father and child node

 public:
    SearchStats() {}

    inline void add_nb_generated() { nb_generated_++; }
    inline void add_nb_expanded() { nb_expanded_++; }
    inline void add_nb_trashed() { nb_trashed_++; }
    inline void add_nb_pruned() { nb_pruned_++; }
    inline void add_eval() { nb_eval_++; }
    inline void add_guide() { nb_guide_++; }
    inline NodeNumber get_nb_generated() const { return nb_generated_; }
    inline NodeNumber get_nb_expanded() const { return nb_expanded_; }
    inline NodeNumber get_nb_trashed() const { return nb_trashed_; }
    inline NodeNumber get_nb_pruned() const { return nb_pruned_; }
    inline NodeNumber get_nb_eval() const { return nb_eval_; }
    inline NodeNumber get_nb_guide() const { return nb_guide_; }
    inline double get_avg_branching_factor() const { return static_cast<double>(nb_generated_)/nb_expanded_; }
    bool noObjRegistered() { return primal_curve_.empty(); }
    AnytimePoint lastObjRegistered() { return primal_curve_.getLast(); }
    AnytimeCurve getAnytimeCurve() { return primal_curve_; }
    void addNodeEvent(NodeOpeningEvent e) { node_events_.push_back(e); }
    std::vector<NodeOpeningEvent>& getNodeEvents() { return node_events_; }

    void add_guidediff_at_depth(Depth d, double v) {
        assert(d <= 1e6);
        assert(d >= 0);
        while ( static_cast<int>(guidediff_depth_.size()) < d+1 ) {  // add depths if needed
            guidediff_depth_.push_back(AvgNumber());
        }
        guidediff_depth_[d].addValue(v);
    }

    void addObj(double time, double v) {
        primal_curve_.addPoint({
            .time = time,
            .nb_generated = get_nb_generated(),
            .nb_expanded = get_nb_expanded(),
            .nb_trashed = get_nb_trashed(),
            .nb_pruned = get_nb_pruned(),
            .nb_eval = get_nb_eval(),
            .nb_guide = get_nb_guide(),
            .v = v
        });
    }

    void addFacticeObj(double time) {
        if ( !primal_curve_.empty() ) {
            addObj(time, primal_curve_.getLast().v);
        }
    }

    void printStats() const {
        std::cout << "nb generated\t\t" << dotNumber(get_nb_generated()) << std::endl;
        std::cout << "nb expanded\t\t" << dotNumber(get_nb_expanded()) << std::endl;
        std::cout << "nb trashed\t\t" << dotNumber(get_nb_trashed()) << std::endl;
        std::cout << "nb pruned\t\t" << dotNumber(get_nb_pruned()) << std::endl;
        std::cout << "avg branching factor\t" << getDoubleAtPrecision(get_avg_branching_factor(), 2) << std::endl;
        std::cout << "nb evaluations\t\t" << dotNumber(get_nb_eval()) << std::endl;
        std::cout << "nb guide calls\t\t" << dotNumber(get_nb_guide()) << std::endl;
    }

    std::string jsonStats() const {
        std::string res = "{";
        res += "\"nbgenerated\": " + std::to_string(get_nb_generated()) + ",";
        res += "\"nbexpanded\": " + std::to_string(get_nb_expanded()) + ",";
        res += "\"nbtrashed\": " + std::to_string(get_nb_trashed()) + ",";
        res += "\"nbpruned\": " + std::to_string(get_nb_pruned()) + ",";
        res += "\"avgbranching\": " + std::to_string(get_avg_branching_factor()) + ",";
        res += "\"nbevalcalls\": " + std::to_string(get_nb_eval()) + ",";
        res += "\"nbguidecalls\": " + std::to_string(get_nb_guide());
        if ( !primal_curve_.empty() ) {
            res += ",\"bestval\": " + std::to_string(primal_curve_.getLast().v);
        }
        
        // add guidediff depth
        // res += "\"guidediff_depth\": [";
        // for ( int i = 0 ; i < static_cast<int>(guidediff_depth_.size()) ; i++ ) {
        //     res += "{\"avg\":" + std::to_string(guidediff_depth_[i].avg);
        //     res += ",\"n\":" + std::to_string(guidediff_depth_[i].n) + "}";
        //     res += ( i < static_cast<int>(guidediff_depth_.size())-1 ) ? "," : "";
        // }
        // res += "]";
        res += "}";
        return res;
    }

    /**
     * \brief writes jsonStats() to a file
     */
    void writeJsonStats(std::string filename) {
        std::ofstream f;
        f.open(filename);
        f << jsonStats();
        f.close();
    }

};

}  // namespace cats