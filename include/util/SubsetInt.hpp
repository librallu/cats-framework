#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <cassert>
#include <boost/functional/hash.hpp>

namespace cats {

/**
 * \brief provides an efficient implementation of a scalable subset of integers including hash functions
 */
struct SubsetInt {
    std::vector<uint64_t> field;

    /**
     * \brief copy constructor
     */
    SubsetInt(const SubsetInt& p) {
        field = std::vector<uint64_t>();
        for (uint64_t e : p.field) {
            field.push_back(e);
        }
    }

    /**
     * \brief initializes an empty set
     */
    explicit SubsetInt(bool full = false) {
        field = std::vector<uint64_t>();
        if ( full ) {
            field.push_back(uint64_t(-1));
        } else {
            field.push_back(uint64_t(0));
        }
    }

    /**
     * \brief initializes the set with a vector of int
     */
    explicit SubsetInt(const std::vector<int>& vect) {
        field = std::vector<uint64_t>();
        field.push_back(uint64_t(0));
        for (auto& i : vect) add(i);
    }

    /**
     * \brief Constructs a set with a vector of uint64_t
     */
    explicit SubsetInt(const std::vector<uint64_t>& f) : field(f) {}

    /**
     * \brief hash function of the subset
     */
    size_t hash() const {
        size_t res = 0;
        for ( auto e : field ) {
            boost::hash_combine(res, e);
        }
        return res;
    }


    /**
     * \brief returns true if the subset contains v
     */
    inline bool contains(int v) const {
        assert(v >= 0);
        if (v/64 < static_cast<int>(field.size()))
            return ((uint64_t(1) << (v%64)) & field[v/64]);
        return false;
    }


    /**
     * \brief returns true if p is included inside the subset
     */
    inline bool include(const SubsetInt& p) {
        if (field.size() < p.field.size())
            return false;
        for (int index = 0 ; index < static_cast<int>(p.field.size()) ;  index++)
            if ((field[index] | p.field[index]) > field[index])
                return false;
        return true;
    }


    /**
     * \brief adds v to the subset
     */
    inline void add(int v) {
        assert(v >= 0);
        int bucket_id = v/64;
        while ( static_cast<int>(field.size()) <= bucket_id ) {  // if needed, increase number of buckets
            field.push_back(uint64_t(0));
        }
        uint64_t translate = 1;
        translate = translate << (v%64);
        field[bucket_id] |= translate;
    }


  /**
   * \brief removes v if it belongs to the subset
   */
    inline void remove(int v) {
        assert(v >= 0);
        int bucket_id = v/64;
        uint64_t translate = 1;
        translate = translate << (v%64);
        field[bucket_id] &= ~translate;
    }

    /**
     * \brief returns true if empty
     */
    inline bool empty() const {
        for ( auto e : field ) {
            if ( e > 0 ) return false;
        }
        return true;
    }

    /**
     * \brief returns the intersection with another set
     */
    inline SubsetInt intersection(const SubsetInt& s) const {
        int n = std::min(field.size(), s.field.size());
        std::vector<uint64_t> res;
        for ( int i = 0 ; i < n ; i++ ) {
            res.push_back(field[i] & s.field[i]);
        }
        // clear excess elements
        while ( res.size() > 0 && res[res.size()-1] == 0 ) res.pop_back();
        return SubsetInt(res);
    }

    /**
     * \brief returns the union with another set
     */
    inline SubsetInt getUnion(const SubsetInt& s) const {
        int n = std::max(field.size(), s.field.size());
        std::vector<uint64_t> res;
        for ( int i = 0 ; i < n ; i++ ) {
            res.push_back(field[i] | s.field[i]);
        }
        return SubsetInt(res);
    }

    /**
     * \brief returns true iff the intersection is empty
     */
    inline bool interEmpty(const SubsetInt& s) const {
        int n = std::min(field.size(), s.field.size());
        for ( int i = 0 ; i < n ; i++ ) {
            if ( (field[i] & s.field[i]) > 0 ) return false;
        }
        return true;
    }


    /**
     * \brief outputs the subset p
     */
    friend std::ostream &operator<<(std::ostream &output, const SubsetInt &P) {
        for (int i = 0 ; i < static_cast<int>(P.field.size()) ; i++) {
            uint64_t t = 1;
            for (int j = 0 ; j < 64 ; j++) {
                if (j != 0)
                    t = t << 1;
                if ((t&P.field[i]) != (uint64_t(0))) {
                    output << j << ".";
                }
            }
        }
        return output;
    }

    /**
     * \brief returns true if both subsets contain the same values
     * TODO(luc) make sure that adding + deleting "blocks" maintain equality
     */
    bool operator==(const SubsetInt& p) const {
        if (p.field.size() != field.size()) return false;
        for (int i = 0 ; i < static_cast<int>(field.size()) ; i++) {
            if (field[i] != p.field[i]) return false;
        }
        return true;
    }
};


}  // namespace cats
