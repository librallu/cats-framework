#pragma once

#include <cassert>
#include <cmath>
#include <vector>

#include "../ReinforcementLearning.hpp"

namespace cats {
namespace rl {

/**
 * encodes necessary information about a slot for UCB
 */
struct UCBInfo {
    Reward q = 0.;
    Nbtrials n = 1.;
};

/**
 * \brief implements a UCB algorithm
 */
class UCB : public BanditAlgorithm {
 protected:
    std::vector<UCBInfo> slots_;   ///< slot machines informations
    Nbtrials n_;   ///< total trials
    double c_;   ///< diversification factor

 public:
    /**
     * \param n slot number
     */
    UCB(int n, double c=1.) : slots_(n), n_(1), c_(c) {}

    /**
     * \brief let UCB choose the next action to take
     */
    Action choice() override {
        Action res = 0;
        double maxi = UCBScore(0);
        for ( int i = 1 ; i < static_cast<int>(slots_.size()) ; i++ ) {
            double s = UCBScore(i);
            if ( s > maxi ) {
                res = i;
                maxi = s;
            }
        }
        return res;
    }

    /**
     * \brief updates UCB knowledge. Inform that taking the action a produced reward r
     */
    void update(Action a, Reward r) {
        assert(a < static_cast<int>(slots_.size()));
        slots_[a].q = updateAverage(slots_[a].q, r, slots_[a].n);
        n_++;
        slots_[a].n++;
    }

    /**
     * \brief reinitializes UCB to initial values
     */
    void reinitialize() override {
        n_ = 1;
        for ( UCBInfo& e : slots_ ) {
            e.q = 0;
            e.n = 1;
        }
    }

 protected:
    /**
     * gets UCB score of lever i
     * \param i slot i
     */
    double UCBScore(int i) {
        return slots_[i].q + c_*sqrt(log(n_)/slots_[i].n);
    }
};


};  // namespace rl
};  // namespace cats