#pragma once

#include <cassert>
#include <cmath>
#include <vector>

#include "../../numeric.hpp"
#include "../ReinforcementLearning.hpp"

namespace cats {
namespace rl {

/**
 * encodes necessary information about a slot for Epsilon greedy algorithm
 */
struct EgreedyInfo {
    Reward q = 0.;
    Nbtrials n = 0;
};

/**
 * \brief implements a Egreedy algorithm
 */
class Egreedy : public BanditAlgorithm {
 protected:
    std::vector<EgreedyInfo> slots_;   ///< slot machines informations
    double e_;   ///< epsilon (diversification factor)
    double qinit_;   ///< initial reward (optimistic reward)

 public:
    /**
     * \param n slot number
     */
    Egreedy(int n, double e, double qinit) : slots_(n, {qinit, 0}), e_(e), qinit_(qinit) {}

    /**
     * \brief let Egreedy choose the next action to take
     */
    Action choice() override {
        // random choice
        if ( randomFloat() < e_ ) {
            return randomInt(0, slots_.size());
        }
        // greedy choice
        Action res = 0;
        double maxi = EgreedyScore(0);
        for ( int i = 1 ; i < static_cast<int>(slots_.size()) ; i++ ) {
            double s = EgreedyScore(i);
            if ( s > maxi ) {
                res = i;
                maxi = s;
            }
        }
        return res;
    }

    /**
     * \brief updates Egreedy knowledge. Inform that taking the action a produced reward r
     */
    void update(Action a, Reward r) {
        assert(a < static_cast<int>(slots_.size()));
        slots_[a].n++;
        slots_[a].q = updateAverage(slots_[a].q, r, slots_[a].n);
    }

    /**
     * \brief reinitializes Egreedy to initial values
     */
    void reinitialize() override {
        for ( EgreedyInfo& e : slots_ ) {
            e.q = qinit_;
            e.n = 0;
        }
    }

 protected:
    /**
     * gets Egreedy score of lever i
     * \param i slot i
     */
    double EgreedyScore(int i) {
        return slots_[i].q;
    }
};


};  // namespace rl
};  // namespace cats