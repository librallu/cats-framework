#pragma once

#include <cassert>
#include <cmath>
#include <vector>

#include "../../numeric.hpp"
#include "../ReinforcementLearning.hpp"

namespace cats {
namespace rl {

/**
 * encodes necessary information about a slot for a Gradient Bandit algorithm
 */
struct GradientBanditInfo {
    double h;
    double p;
    Nbtrials n = 0;
};

/**
 * \brief implements a Gradient Bandit algorithm algorithm
 */
class GradientBandit : public BanditAlgorithm {
 protected:
    std::vector<GradientBanditInfo> slots_;   ///< slot machines informations
    double baseline_;  ///< average reward over time
    Nbtrials n_;  ///< nb trials
    double step_size_;  ///< learning step size
    bool use_baseline_;  ///< if true, use baseline

 public:
    /**
     * \param n slot number
     */
    GradientBandit(int n, double step_size, bool use_baseline) : slots_(n, {0, 1./n, 0}),
    baseline_(0.), n_(0), step_size_(step_size), use_baseline_(use_baseline) {}

    /**
     * \brief let GradientBandit choose the next action to take
     */
    Action choice() override {
        int res = 0;
        double r = randomFloat();
        double acc = slots_[res].p;
        while ( acc < r ) {
            res++;
            assert(res < static_cast<int>(slots_.size()));
            acc += slots_[res].p;
        } 
        return res;
    }

    /**
     * \brief updates GradientBandit knowledge. Inform that taking the action a produced reward r
     */
    void update(Action action, Reward r) {
        assert(action < static_cast<int>(slots_.size()));
        // update baseline
        if ( use_baseline_ ) {
            n_++;
            baseline_ = updateAverage(baseline_, r, n_);
        }
        // update h
        double sum_h = 0.;
        for ( Action a = 0 ; a < static_cast<int>(slots_.size()) ; a++ ) {
            if ( a == action ) {
                slots_[a].h += step_size_ * (r - baseline_) * (1-slots_[a].p);
            } else {
                slots_[a].h -= step_size_ * (r - baseline_) * slots_[a].p;
            }
            sum_h += exp(slots_[a].h);
        }
        // update p
        for ( Action a = 0 ; a < static_cast<int>(slots_.size()) ; a++ ) {
            slots_[a].p = exp(slots_[a].h)/sum_h;
        }
    }

    /**
     * \brief reinitializes Egreedy to initial values
     */
    void reinitialize() override {
        for ( GradientBanditInfo& e : slots_ ) {
            e.h = 0;
            e.p = 1./static_cast<double>(slots_.size());
            e.n = 0;
        }
        n_ = 0;
        baseline_ = 0.;
    }
};


};  // namespace rl
};  // namespace cats