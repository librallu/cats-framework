#pragma once

#include <vector>

/**
 * # Implements several Reinforcement learning procedures.
 * 
 * ## Bandit algorithms
 * Given a set of possible actions, chose the one that maximizes the profit in the long run (diversification vs intensification)
 * The learned values can be policies or Q-values.
 *
 */

namespace cats {
namespace rl {

typedef uint64_t Nbtrials;
typedef int Action;
typedef double Reward;

/**
 * \param a past average reward
 * \param r current reward
 * \param n nb trials (last one included)
 */
double updateAverage(double a, double r, Nbtrials n) {
    return a + (r-a)/n;
}

class BanditAlgorithm {
 public:
    /**
     * \brief returns next action to be chosen.
     */
    virtual Action choice() = 0;

    /**
     * \brief updates the bandit algorithm
     */
    virtual void update(Action a, Reward r) = 0;

    /**
     * \brief reinitializes the bandit algorithm
     */
    virtual void reinitialize() = 0;
};


};  // namespace rl
};  // namespace cats
