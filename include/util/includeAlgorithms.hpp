#pragma once

/**
 * \file includeAlgorithms.hpp Includes all tree search algorithms and combinators at once
 */

/* Tree Search algorithms */
#include "../treesearch/DFS.hpp"
#include "../treesearch/BFS.hpp"
#include "../treesearch/AStar.hpp"
#include "../treesearch/MBAStar.hpp"
#include "../treesearch/IterativeMBAStar.hpp"
#include "../treesearch/BeamSearch.hpp"
#include "../treesearch/IterativeBeamSearch.hpp"
#include "../treesearch/GreedyRandom.hpp"
#include "../treesearch/LDS.hpp"
#include "../treesearch/BULB.hpp"
#include "../treesearch/IterativeBroadening.hpp"
#include "../treesearch/WeightedAStar.hpp"
#include "../treesearch/Greedy.hpp"
#include "../treesearch/BranchAndGreed.hpp"
#include "../treesearch/ACO.hpp"
#include "../treesearch/IterativeRouletteWheelGreedy.hpp"
#include "../treesearch/MCTS.hpp"
#include "../treesearch/AnytimeColumnSearch.hpp"
#include "../treesearch/APS.hpp"


/* Tree Search Combinators */
#include "../Combinator.hpp"  // can be used as dummy combinator
#include "../combinators/LDSCombinator.hpp"
#include "../combinators/PrefixEquivalenceCombinator.hpp"
#include "../combinators/IterativeBroadeningCombinator.hpp"
#include "../combinators/BeamCombinator.hpp"
#include "../combinators/ProbingCombinator.hpp"
#include "../combinators/WeightedCombinator.hpp"
#include "../combinators/StatsCombinator.hpp"
#include "../combinators/DominanceCombinator.hpp"
#include "../combinators/DominanceCombinator2.hpp"
#include "../combinators/BBCombinator.hpp"
#include "../combinators/BucketListCombinator.hpp"

