#pragma once

#include <exception>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>

typedef int64_t bigint;

namespace cats {

/**
 * \brief fired when a file is not found while trying to open it
 */
class FileNotFoundException: public std::exception {
  virtual const char* what() const throw() {
    return "File not found exception";
  }
};


/**
 * \brief fired when a file is not openable
 */
class UnableToOpenFileException: public std::exception {
  virtual const char* what() const throw() {
    return "Unable to open file";
  }
};

/** \brief exception indicating that a function is not implemented while called
 */
struct NotImplementedException : public std::exception {
    const char* what() const throw() {
        return "NotImplementedException: method not implemented";
    }
};

/**
 * \brief enum of colors
 */
enum Color {Black, Red, Green, Orange, Blue, Purple, Cyan, Yellow};

/**
 * \brief number printer Helper function. *Ex: 1000 -> 1 K, 1000000 -> 1 M*
 */
std::string printBigInt(bigint e) {
    if ( e < 1000 ) {
        return std::to_string(static_cast<int64_t>(e));
    } else if ( e < 1000000 ) {
        return std::to_string(static_cast<int64_t>(e)/1000)+std::string(" K");
    } else {
        return std::to_string(static_cast<int64_t>(e)/1000000)+std::string(" M");
    }
}



/**
 * \brief returns a colored text (works on UNIX terminals)
 */
std::string stringColor(Color c, std::string text) {
    std::string res = "";
    switch (c) {
        case Black: res += "\033[1;30m"; break;
        case Red: res += "\033[1;31m"; break;
        case Green: res += "\033[1;32m"; break;
        case Orange: res += "\033[1;33m"; break;
        case Blue: res += "\033[1;34m"; break;
        case Purple: res += "\033[1;35m"; break;
        case Cyan: res += "\033[1;36m"; break;
        case Yellow: res += "\033[1;33m"; break;
    }
    return res + text + "\033[0m";
}


/**
 * \brief put dots in number. *Ex: 1000 -> 1.000*
 */
std::string dotNumber(int n) {
    if ( n < 1000 ) {
        return std::to_string(n);
    } else {
        std::string res = "";
        int a = n%1000;
        if ( a < 10 ) res += std::string("0");
        if ( a < 100 ) res += std::string("0");
        return dotNumber(n/1000)+"."+res+std::to_string(a);
    }
}


/**
 * \brief get double to precision. *Ex: getDoubleAtPrecision(42.1337, 3) = 42.133*
 */
std::string getDoubleAtPrecision(double v, int p) {
    std::stringstream res;
    res.precision(p);
    res << std::fixed << v;
    return res.str();
}


/**
 * \brief converts a double to a string
 */
std::string doubleToString(double number) {
    std::ostringstream buff;
    buff << number;
    return buff.str();
}


/**
 * \brief returns a formatted title
 * ex: title1String("TITLE", 19) returns
 *  <- 19 characters ->
 * "====== TITLE ======"
 */
std::string title1String(std::string title, int size, std::string delim) {
    std::string res = "";
    int title_size = static_cast<int>(title.size())+2;
    int delim1_size = floor((size-static_cast<float>(title_size))/2);
    int delim2_size = ceil((size-static_cast<float>(title_size))/2);
    for ( int i = 0 ; i < delim1_size ; i++ ) {
        res += delim;
    }
    res += std::string(" ")+title+std::string(" ");
    for ( int i = 0 ; i < delim2_size ; i++ ) {
        res += delim;
    }
    return res;
}


/**
 * \brief return arrays in strings
 * ex: printArray<int>([1, 2, 5]) returns "[1 2 5]"
 */
template <typename T>
std::string stringArray(std::vector<T> a) {
    std::ostringstream buff;
    buff << "[";
    for ( T t : a ) {
        buff << t << " ";
    }
    buff << "]";
    return buff.str();
}


}  // namespace cats
