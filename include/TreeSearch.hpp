#pragma once

#include <string>
#include <set>

#include "Node.hpp"
#include "SearchManager.hpp"
#include "numeric.hpp"

namespace cats {

/**
 * \brief Compares two nodes by their guide. If a tie, compare prefixes
 */
struct Compare {
    SearchManager& manager_;

    explicit Compare(SearchManager& manager): manager_(manager) {}

    bool operator()(const NodePtr a, const NodePtr b) const {
        double guide_a = a.get()->guide();
        double guide_b = b.get()->guide();
        return manager_.isStrictlyBetter(guide_a, guide_b);
    }
};

/**
 * \brief provides generic tree search parameters
 */
struct TreeSearchParameters {
    NodePtr root;  ///< root node. To be copied at the beginning of each run
    SearchManager& manager;  ///< reference of the search manager
    int id;   ///< id of algorithm
};

/** \class TreeSearch TreeSearch.hpp
 *  \brief This Abstract class is used to provide a base code and interface for search algorithms.
 */
class TreeSearch {

 protected:
    NodePtr root_;  ///< root node. To be copied at the beginning of each run
    NodePtr local_best_;  ///< best solution found so var by the current method
    double local_best_value_;  ///< local best solution value
    SearchManager& manager_;  ///< reference of the search manager
    int id_;   ///< id of algorithm
    bool do_check_dominance_;   ///< indicates if the algorithm can cut nodes dominated by best solution
    bool measure_heuristic_cuts_ = true;   ///< if true, compute global lb
    float bound_cut_threshold_ = 0.;   ///< indicates the percentage of allowed acceptance. (i.e, in a minimization problem, if the best known is 100 and we allow 10%, solutions below 110 wont be cut)
    Compare compare_;   ///< node comparison object

 public:
    explicit TreeSearch(TreeSearchParameters p)
    : root_(p.root.get()->copy()), local_best_(NodePtr(NULL)), manager_(p.manager),
    id_(p.id), do_check_dominance_(true), compare_(p.manager) {
        local_best_value_ = manager_.getBoundPrimal();
    }

    /**
     * \brief sets measure_heuristic_cuts (used to disable global lower bound check)
     */
    void set_measure_heuristic_cuts(bool v) {
        measure_heuristic_cuts_ = v;
    }

    /**
     * \brief called when search terminates. Dealocate objects relative to search
     */
    void terminate() {}

    /**
     * \brief enables or disables the dominance by best solution found so far (desactivate if probing)
     */
    virtual inline void set_check_dominance(bool a) { do_check_dominance_ = a; }

    /**
     * \brief sets the cut threshold
     */
    virtual inline void set_cut_threshold(float v) {
        assert(v >= 0.);
        bound_cut_threshold_ = v;
    }

    /**
     * \brief displays algorithm information (name, parameters and possibly more)
     */
    virtual std::string algorithmMessage() const = 0;

    /**
     * \brief run algorithm for a given time
     * \param time_limit time in seconds at which the search should end
     */
    virtual NodePtr run(double time_limit) = 0;

    /**
     * \brief returns true if *s* is dominated by the best solution found so far (bound cut)
     */
    inline bool isDominatedByBest(NodePtr s) {
        return do_check_dominance_ && manager_.isDominatedByBest(s, bound_cut_threshold_);
    }

    /**
     * \brief reinitializes lower bound informations of the algorithm
     */
    inline void resetDual() {
    }

    /**
     * \brief add a lower bound to fringe
     */
    inline void addDualtoFringe(double v) {
    }

    /**
     * \brief removes a lower bound of fringe
     */
    inline void removeDualFromFringe(double v) {
    }

    /**
     * \brief registers a heuristic cut
     */
    inline void performHeuristicCut(double v) {
    }


    /**
     * \brief checks admissibility and compare with best
     */
    void admissibleAndCompare(NodePtr s) {
        if ( s.get()->isGoal() ) {
            compareWithBest(s);
        }
    }


    /**
     * \brief Default method used when we find a goal during the search. If improving, update best and number of solutions found.
     */
    void compareWithBest(NodePtr s) {
        // update local best
        assert(s.get()->isGoal());
        double eval_s = s.get()->evaluate();
        bool local_best_found = !(local_best_.get() == NULL);
        bool comparison = false;
        if (local_best_found) {
            comparison = manager_.isStrictlyBetter(eval_s, local_best_value_);
        }
        if (!local_best_found || comparison) {
            local_best_ = s;
            local_best_value_ = eval_s;
            manager_.compareWithBest(local_best_, algorithmMessage(), id_);
        }
    }
};

}  // namespace cats
