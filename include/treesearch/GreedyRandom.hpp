#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"
#include "../numeric.hpp"

namespace cats {

/** \class GreedyRandom
 * \brief Implementation of a greedy random algorithm
 */
class GreedyRandom : public TreeSearch {
 private:
  float last_alpha_;
  bool only_one_iteration_;

  inline double evalNode(NodePtr s, float alpha) {
    return (alpha*s.get()->evaluate())+(1-alpha)*randomFloat();
  }

 public:
    std::string algorithmMessage() const override {
        float e = static_cast<int>(last_alpha_ * 100)/100.;
        return std::string("GR(id:") + std::to_string(id_) + std::string(", ") + std::to_string(e) + std::string(")");
    }

    void doOnlyOneIteration(bool v) {
        only_one_iteration_ = v;
    }

    explicit GreedyRandom(TreeSearchParameters params, bool only_one_iteration = false)
        : TreeSearch(params), last_alpha_(1.), only_one_iteration_(only_one_iteration) {}

    NodePtr run(double time_limit) override {
        while ( !manager_.searchEnded(time_limit) ) {
            NodePtr current = root_;
            double alpha = randomFloat();  // generate a parameter indicating the part of random
            last_alpha_ = alpha;
            while ( !current.get()->isGoal() && !manager_.searchEnded(time_limit) && !isDominatedByBest(current) ) {
                std::vector<NodePtr> children = current.get()->getChildren();
                if ( children.size() == 0 ) break;
                // initialize comparison value
                double eval_gr = evalNode(children.front(), alpha);
                current = children.front();
                // looking for the node with lowest GRA value
                for (NodePtr child : children) {
                    double eval_gr2 = evalNode(child, alpha);
                    if ( manager_.isBetter(eval_gr2, eval_gr) ) {
                        eval_gr = eval_gr2;
                        current.swap(child);
                    }
                }
            }
            if (current.get()->isGoal())
                compareWithBest(current);
            if ( only_one_iteration_ ) {
                return local_best_;
            }
        }
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
