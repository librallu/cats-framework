#pragma once

#include <list>
#include <string>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

/** \class BFS BFS.hpp
 *  \brief Implements a BFS algorithm.
 */
class BFS : public TreeSearch {
 public:

  explicit BFS(TreeSearchParameters params)
        : TreeSearch(params) {}

  std::string algorithmMessage() const override {
    return std::string("BFS(id:") + std::to_string(id_) + std::string(")");
  }

  NodePtr run(double time_limit) override {
        // Initialization of tree exploration
        std::list<NodePtr> stack;
        stack.push_back(root_);
        // explore while tree is not empty and time is not over
        while ( stack.size() > 0 && !manager_.searchEnded(time_limit) ) {
            NodePtr current = stack.front();
            stack.pop_front();
            // std::cout << *((BranchingSchemeTSP*)(current.get()));
            // lower bound cut
            if ( isDominatedByBest(current) ) continue;
            // if valid solution
            if ( current.get()->isGoal() ) {
                compareWithBest(current);
            } else {  // else, add children to stack
                for ( NodePtr child : current.get()->getChildren() ) {
                    // lower bound cut
                    if ( isDominatedByBest(child) ) continue;
                    // add child to stack
                    stack.push_back(child);
                }
            }
        }
        this->terminate();
        return local_best_;
    }

};

}  // namespace cats

