#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>


#include "DFS.hpp"
#include "../combinators/LDSCombinator.hpp"
#include "../combinators/BeamCombinator.hpp"
#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \brief Implements BULB which is a LDS + Beam Combinator
 */
class BULB : public TreeSearch {
 private:
    std::string msg_;
    int d_;

    void construct_message(int d) {
        msg_ = std::string("BULB(id:") + std::to_string(id_) + std::string(",") + std::to_string(d_) + std::string(",") + std::to_string(d) + std::string(")");
    }

 public:

    explicit BULB(TreeSearchParameters params, int d):
      TreeSearch(params), msg_("no run yet"), d_(d) {}

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        // time checking
        double d = 0;
        while ( !manager_.searchEnded(time_limit) ) {
            NodePtr beam_root = NodePtr(new BeamCombinator<>(root_, manager_, d_));
            NodePtr root = NodePtr(new LDSCombinator<>(beam_root, manager_, d++));
            DFS algorithm({.root=root, .manager=manager_, .id=id_});
            algorithm.set_check_dominance(do_check_dominance_);
            algorithm.set_measure_heuristic_cuts(measure_heuristic_cuts_);
            construct_message(d);
            NodePtr best_alg = algorithm.run(time_limit);
            if ( best_alg != nullptr && best_alg.get() != nullptr ) {
              if ( local_best_.get() == nullptr || best_alg.get()->evaluate() < local_best_.get()->evaluate() ) {
                local_best_ = best_alg;
              }
            }
        }
        return local_best_;
    }
};

}  // namespace cats
