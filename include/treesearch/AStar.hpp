#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>
#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

/** \class AStar AStar.hpp
 *  \brief Implements a A* tree search algorithm.
 */
class AStar : public TreeSearch {
 public:
 
    explicit AStar(TreeSearchParameters params)
        : TreeSearch(params) {}

    std::string algorithmMessage() const override {
        return std::string("A*(id:") + std::to_string(id_) + std::string(")");
    }

    NodePtr run(double time_limit) override {
        // initialize front with root
        std::multiset<NodePtr, Compare> front(compare_);
        front.insert(root_);
        while ( !front.empty() && !manager_.searchEnded(time_limit) ) {
            NodePtr s = *front.begin();
            front.erase(front.begin());
            // lower bound cut
            if ( isDominatedByBest(s) ) {
                continue;
            }
            // if admissible, check if better than best sol
            if ( s.get()->isGoal() ) {
                compareWithBest(s);
                break;
            }
            for ( NodePtr t : s.get()->getChildren() ) {
                if ( isDominatedByBest(t) ) continue;
                front.insert(t);
            }
        }
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
