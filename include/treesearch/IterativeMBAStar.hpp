#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <string>
#include <omp.h>
#include <limits>


#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"
#include "MBAStar.hpp"

namespace cats {

/**
 * \class IterativeMBAStar
 * \brief Implements an iterative MBA* algorithm.
 * It runs several MBA* with an initial fringe size of *d* and multiplies it by *gf* at every iteration.
 */
class IterativeMBAStar : public TreeSearch {
 private:
    int d_;
    float gf_;
    std::string msg_;
    bool autorize_end_;

 public:

    IterativeMBAStar(TreeSearchParameters params, int d, float gf, bool autorize_end = true):
      TreeSearch(params), d_(d), gf_(gf), msg_("no run yet"), autorize_end_(autorize_end) {}

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        // time checking
        uint64_t d = d_;
        while ( !manager_.searchEnded(time_limit) ) {
            // std::cout << "Start MBA* w=" << static_cast<int>(d) << std::endl;
            MBAStar algorithm({.root = root_, .manager = manager_, .id = id_}, d, autorize_end_);
            algorithm.set_check_dominance(do_check_dominance_);
            algorithm.set_measure_heuristic_cuts(measure_heuristic_cuts_);
            msg_ = algorithm.algorithmMessage();
            root_.get()->setTreeSearchName(&msg_);
            NodePtr best_alg = algorithm.run(time_limit);
            root_.get()->newIter();
            if ( best_alg != nullptr && best_alg.get() != nullptr ) {
              if ( local_best_.get() == nullptr || best_alg.get()->evaluate() < local_best_.get()->evaluate() ) {
                local_best_ = best_alg;
              }
            }
            d *= gf_;
            if ( d == d*gf_ ) {
                d++;
            }
        }
        return local_best_;
    }
};

}  // namespace cats
