#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>


#include "DFS.hpp"
#include "../combinators/LDSCombinator.hpp"
#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \class LDS
 * \brief Implements a LDS algorithm. It runs successive DFS with a LDS combinator with discrepancies increasing
 */
class LDS : public TreeSearch {
 private:
    DiscrepancyEvolution type_;
    std::string msg_;

    void construct_message(int d) {
        msg_ = std::string("LDS(id:") + std::to_string(id_) + std::string(",") + std::to_string(d) + std::string(")");
    }

 public:

    explicit LDS(TreeSearchParameters params, DiscrepancyEvolution type = DiscrepancyEvolution::LIN) :
      TreeSearch(params), type_(type), msg_("no run yet") {}

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        // time checking
        double d = 0;
        while ( !manager_.searchEnded(time_limit) ) {
            NodePtr root = NodePtr(new LDSCombinator<>(root_, manager_, d++, type_));
            construct_message(d);
            root.get()->setTreeSearchName(&msg_);
            DFS algorithm({.root = root, .manager = manager_, .id = id_});
            algorithm.set_check_dominance(do_check_dominance_);
            algorithm.set_measure_heuristic_cuts(measure_heuristic_cuts_);
            NodePtr best_alg = algorithm.run(time_limit);
            root_.get()->newIter();
            if ( best_alg != nullptr && best_alg.get() != nullptr ) {
              if ( local_best_.get() == nullptr || best_alg.get()->evaluate() < local_best_.get()->evaluate() ) {
                local_best_ = best_alg;
              }
            }
        }
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
