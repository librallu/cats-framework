#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"
#include "../numeric.hpp"
#include "RouletteWheelGreedy.hpp"

namespace cats {

/** \class IterativeRouletteWheelGreedy
 * \brief Implementation of an iterative roulette wheel greedy
 */
class IterativeRouletteWheelGreedy : public TreeSearch {
 private:
  float last_alpha_;

 public:
    std::string algorithmMessage() const override {
        return std::string("IRW(id:") + std::to_string(id_) + std::string(")");
    }


    explicit IterativeRouletteWheelGreedy(TreeSearchParameters params) : TreeSearch(params) {}

    NodePtr run(double time_limit) override {
        std::string msg = algorithmMessage();
        while ( !manager_.searchEnded(time_limit) ) {
            root_.get()->setTreeSearchName(&msg);
            RouletteWheelGreedy ts = RouletteWheelGreedy({root_, manager_, id_});
            ts.run(time_limit);
        }
        return local_best_;
    }
};

}  // namespace cats
