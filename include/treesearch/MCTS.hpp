#pragma once

#include <list>
#include <string>
#include <iostream>
#include <algorithm>
#include <math.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"
#include "../numeric.hpp"

#include "RouletteWheelGreedy.hpp"


namespace cats {

typedef uint64_t NbExplorations;

struct MCTSNode;

/**
 * \brief MCTSNode encapsulates a Node and stores additional statistics used within the MCTS
 */
struct MCTSNode {
    NodePtr node;  ///< current Node
    MCTSNode* parent;   ///< MCTSNode parent
    std::vector<MCTSNode*> children;  ///< MCTSNode children
    double q;  ///< average quality (to be minimized)
    NbExplorations N;   ///< number of explorations below

    void printInfo() {
        std::cout << "MCTSNode:" << q << "\t" << N << "\n";
    }
};

double evalMCTSNode(MCTSNode* n) {
    double w = 1./n->q;
    double c = 1.414;
    double diversification_factor = 0.;
    if ( n->parent != nullptr ) {
        MCTSNode* p = n->parent;
        diversification_factor = c*sqrt(log(p->N)/n->N);
    }
    return w/n->N + diversification_factor;
}


/** \class MCTS MCTS.hpp
 *  \brief Implements a Monte Carlo Tree Search (MCTS) algorithm.
 */
class MCTS : public TreeSearch {
 private:
    std::string msg_;
    MCTSNode mcts_root_;
    double time_limit_;  ///< time limit

 public:
    std::string algorithmMessage() const override {
        return msg_;
    }

    void set_msg(std::string msg) {
        msg_ = msg;
    }

    explicit MCTS(TreeSearchParameters params)
        : TreeSearch(params), msg_(std::string("MCTS(id:") + std::to_string(id_) + std::string(")")) {}

    NodePtr run(double time_limit) override {
        root_.get()->setTreeSearchName(&msg_);
        time_limit_ = time_limit;

        // constructs initial MCTS root
        initSearch();
        // uint64_t i = 0;

        while ( !manager_.searchEnded(time_limit) ) {
            // selection phase
            // find node that is the most interesting to explore
            MCTSNode* current = findNextNode();

            // expansion phase
            // add selected node children to the explored list
            expandNode(current);

            // simulation phase
            // execute greedy on each child
            simulateNode(current);
        }

        // TODO(all) free memory

        this->terminate();
        return local_best_;
    }

 private:
    /**
     * \brief encapsulates root in a MCTSNode structure
     */
    void initSearch() {
        mcts_root_ = {
            .node = root_,
            .parent = nullptr,
            .children = std::vector<MCTSNode*>(),
            .q = 0.,
            .N = 0
        };
    }

    /**
     * \brief find node that is the most interesting to explore next
     */
    MCTSNode* findNextNode() {
        MCTSNode* res = &mcts_root_;
        assert((res->node.get() == root_.get()));
        while ( !res->children.empty() ) {
            MCTSNode* tmp = res->children[0];
            for ( int i = 1 ; i < static_cast<int>(res->children.size()) ; i++ ) {
                if ( evalMCTSNode(res->children[i]) > evalMCTSNode(tmp) ) {
                    tmp = res->children[i];
                }
            }
            res = tmp;
        }
        return res;
    }

    /**
     * \brief creates children and encapsulates them within a MCTSNode structure
     */
    void expandNode(MCTSNode* n) {
        std::vector<NodePtr> original_children = n->node.get()->getChildren();
        for ( auto oc : original_children ) {
            if ( oc.get()->isGoal() ) {
                compareWithBest(oc);
            }
            MCTSNode* c = new MCTSNode;
            c->node = oc;
            c->parent = n;
            c->children = std::vector<MCTSNode*>();
            c->q = 0.;
            c->N = 0;
            n->children.push_back(c);
        }
    }

    /**
     * \brief simulates all children of n
     */
    void simulateNode(MCTSNode* n) {
        for ( MCTSNode* c : n->children ) {
            TreeSearchParameters ts_params = {.root = c->node, .manager = manager_, .id = id_};
            RouletteWheelGreedy ts = RouletteWheelGreedy(ts_params);
            ts.set_check_dominance(false);
            ts.set_q0(.95);
            // ts.set_q0(randomFloat());
            NodePtr res = ts.run(time_limit_);
            if ( res.get() != nullptr ) {
                compareWithBest(res);
            }
            // execute backpropagation
            backpropagateNode(c, res);
        }
    }

    /**
     * \brief update statistics on node n, its direct successors and its ancestors
     */
    void backpropagateNode(MCTSNode* n, NodePtr res) {
        MCTSNode* current = n;
        double eval_res = MAX_DOUBLE;
        if ( res.get() != nullptr ) {
            eval_res = res.get()->evaluate();
        }
        do {
            current->N++;
            current->q = current->q + (eval_res-current->q)/current->N;
            current = current->parent;
        } while ( current != nullptr );
    }
};

}  // namespace cats
