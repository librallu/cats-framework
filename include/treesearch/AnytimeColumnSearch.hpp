#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \brief Implements an anytime column search (ACS). More information here: https://link.springer.com/chapter/10.1007/978-3-642-35101-3_22
 */
class AnytimeColumnSearch :public TreeSearch {
 private:
    int d_;
    bool autorize_stop_;
    std::string msg_;

 public:

    explicit AnytimeColumnSearch(TreeSearchParameters params, bool autorize_stop = false):
      TreeSearch(params), autorize_stop_(autorize_stop) {
          set_msg();
      }

    void set_msg(std::string msg) {
        msg_ = msg;
    }

    void set_msg() {
        msg_ = std::string("ACS(id:") + std::to_string(id_) + std::string(")");
    }

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        root_.get()->setTreeSearchName(&msg_);  // initialize tree search name

        // create levels vector and add root to the first level
        std::vector<std::multiset<NodePtr, Compare>> levels;
        levels.push_back(std::multiset<NodePtr, Compare>(compare_));
        levels[0].insert(root_);
        // resetDual();
        // addDualtoFringe(root_.get()->evaluate());
        uint64_t w = 1;
        bool continue_search = true;
        while ( continue_search && !manager_.searchEnded(time_limit) ) {  // main loop iterates column by column
            continue_search = false;
            // inner loop explores w nodes by level
            uint64_t i = 0;
            while ( i < levels.size() && !manager_.searchEnded(time_limit) ) {
                if ( !levels[i].empty() ) {
                    continue_search = true;
                    // expand the best w nodes
                    for ( uint64_t j = 0 ; j < w ; j++ ) {
                        if ( levels[i].empty() ) break;
                        if ( manager_.searchEnded(time_limit) ) continue;
                        // remove best from levels[i]
                        NodePtr n = *levels[i].begin();
                        levels[i].erase(levels[i].begin());
                        // create children and place them at level[i+1]
                        for ( NodePtr c : n.get()->getChildren() ) {
                            if ( i == levels.size()-1 ) levels.push_back(std::multiset<NodePtr, Compare>(compare_));  // add level if needed
                            if ( isDominatedByBest(c) ) {
                                continue;
                            } else if ( c.get()->isGoal() ) {
                                compareWithBest(c);
                            } else {
                                levels[i+1].insert(c);
                            }
                        }
                    }
                }
                i++;
            }
            w++;  // anytime column progressive search stragegy, increase w by 1 at each iteration
        }

        // search ends, interact with manager if needed
        bool tree_depleted = levels.empty();
        if ( autorize_stop_ && tree_depleted ) {
            manager_.setEnd();  // sets end if the search depletes the tree
        }
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
