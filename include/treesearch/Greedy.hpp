#pragma once

#include <list>
#include <string>
#include <iostream>
#include <algorithm>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

/** \class Greedy Greedy.hpp
 *  \brief Implements a Greedy algorithm. Explores only the best children until it fails or finds a solution
 */
class Greedy : public TreeSearch {
 private:
    std::string msg_;

 public:
    std::string algorithmMessage() const override {
        return msg_;
    }

    void set_msg(std::string msg) {
        msg_ = msg;
    }

    explicit Greedy(TreeSearchParameters params)
        : TreeSearch(params), msg_(std::string("Greedy(id:") + std::to_string(id_) + std::string(")")) {}

    NodePtr run(double time_limit) override {
        root_.get()->setTreeSearchName(&msg_);
        NodePtr n = root_;
        if ( n.get()->isGoal() ) {
            admissibleAndCompare(n);
        }
        // std::cout << time_limit << std::endl;
        std::vector<NodePtr> children = n.get()->getChildren();
        while ( !children.empty() && !isDominatedByBest(n) && !manager_.searchEnded(time_limit) ) {  // while not at a leaf, explore best child
            // n := min(children)
            n = children[0];
            admissibleAndCompare(n);
            double eval_n = n.get()->guide();
            for ( int i = 1 ; i < static_cast<int>(children.size()) ; i++ ) {
                NodePtr e = children[i];
                double eval_e = e.get()->guide();
                admissibleAndCompare(e);
                if ( manager_.isBetter(eval_e, eval_n) ) {
                    n = e;
                    eval_n = eval_e;
                }
            }
            // children := n.getChildren()
            children = n.get()->getChildren();
        }
        this->terminate();
        return local_best_;
    }

};

}  // namespace cats
