#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>


#include "DFS.hpp"
#include "../combinators/IterativeBroadeningCombinator.hpp"
#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \class IterativeBroadening
 * \brief Implements a Iterative Broadening algorithm. It runs successive DFS with a IterativeBroadening combinator with discrepancies increasing
 */
class IterativeBroadening : public TreeSearch {
 private:
    std::string msg_;

    void constructMessage(int d) {
        msg_ = std::string("Ibroadening(id:") + std::to_string(id_) + std::string(",") + std::to_string(d) + std::string(")");
    }

 public:
 
    explicit IterativeBroadening(TreeSearchParameters params):
      TreeSearch(params), msg_("no run yet") {}

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        // time checking
        double d = 0;
        while ( !manager_.searchEnded(time_limit) ) {
            NodePtr root = NodePtr(new IterativeBroadeningCombinator<>(root_, manager_, d++));
            DFS algorithm({.root = root, .manager = manager_, .id = id_});
            algorithm.set_check_dominance(do_check_dominance_);
            algorithm.set_measure_heuristic_cuts(measure_heuristic_cuts_);
            constructMessage(d);
            root_.get()->setTreeSearchName(&msg_);
            NodePtr best_alg = algorithm.run(time_limit);
            root_.get()->newIter();
            if ( best_alg != nullptr && best_alg.get() != nullptr ) {
              if ( local_best_.get() == nullptr || best_alg.get()->evaluate() < local_best_.get()->evaluate() ) {
                local_best_ = best_alg;
              }
            }
        }
        return local_best_;
    }
};

}  // namespace cats
