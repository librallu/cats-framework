#pragma once

#include <list>
#include <string>
#include <iostream>
#include <algorithm>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"
#include "../numeric.hpp"

namespace cats {

/** \class DFS DFS.hpp
 *  \brief Implements a DFS algorithm.
 *  *Algorithm message:* DFS(id=ID,[S]). ID indicates the id of the algorithm. S appears if the DFS sorts nodes
 */
class DFS : public TreeSearch {
 private:
  bool sorted_;

 public:
  std::string algorithmMessage() const override {
    return std::string("DFS(id:") + std::to_string(id_) + std::string(sorted_ ? ",S" : "") + std::string(")");
  }

  explicit DFS(TreeSearchParameters params, bool sorted = true)
        : TreeSearch(params), sorted_(sorted) {}

  NodePtr run(double time_limit) override {
        // Initialization of tree exploration
        std::string msg = algorithmMessage();
        root_.get()->setTreeSearchName(&msg);
        std::list<NodePtr> stack;
        stack.push_back(root_);
        resetDual();
        addDualtoFringe(root_.get()->evaluate());
        // explore while tree is not empty and time is not over
        while ( stack.size() > 0 && !manager_.searchEnded(time_limit) ) {
            NodePtr current = stack.front();
            stack.pop_front();
            // lower bound cut
            // if (isDominatedByBest(current)) {
            // removeDualFromFringe(current.get()->evaluate());
            // continue;
            // }
            // if valid solution
            if ( current.get()->isGoal() ) {
            compareWithBest(current);
            } else {  // else, add children to stack
            std::vector<NodePtr> children = current.get()->getChildren();
            if ( sorted_ ) {
                std::sort(children.begin(), children.end(), compare_);
                std::reverse(children.begin(), children.end());
            }
            for ( NodePtr child : children ) {
                // if ( isDominatedByBest(child) ) continue;  // lower bound cut
                stack.push_front(child);  // add child to stack
                addDualtoFringe(child.get()->evaluate());
            }
                removeDualFromFringe(current.get()->evaluate());
            }
        }
        this->terminate();
        return local_best_;
    }

};

}  // namespace cats
