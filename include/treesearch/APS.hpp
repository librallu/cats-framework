#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \brief Implements an Anytime Pack Search (APS). More information here: https://sites.google.com/site/satyagautamv/aps-journal.pdf
 */
class AnytimePackSearch :public TreeSearch {
 private:
    int k_;
    std::string msg_;
    bool autorize_stop_;

 public:

    explicit AnytimePackSearch(TreeSearchParameters params, int k, bool autorize_stop = false):
      TreeSearch(params), k_(k), autorize_stop_(autorize_stop) {
          set_msg();
      }

    void set_msg(std::string msg) {
        msg_ = msg;
    }

    void set_msg() {
        msg_ = std::string("APS(id:") + std::to_string(id_) + std::string(")");
    }

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        root_.get()->setTreeSearchName(&msg_);  // initialize tree search name
        std::multiset<NodePtr, Compare> remaining(compare_);  // initialize remaining node list
        remaining.insert(root_);
        std::multiset<NodePtr, Compare> beam(compare_);
        while ( !remaining.empty() && !manager_.searchEnded(time_limit) ) {
            // 1. extract the k_ best nodes of remaining
            int n = 0;
            while ( n < k_ && !remaining.empty() ) {
                beam.insert(*remaining.begin());
                remaining.erase(remaining.begin());
                n++;
            }
            // 2. perform a "beam search" possibly storing nodes within remaining
            while ( !beam.empty() && !manager_.searchEnded(time_limit) ) {  // for each level of the tree
                // Beam Iteration: compute list of child nodes from the current level
                std::multiset<NodePtr, Compare> children(compare_);
                double least_children_guide = manager_.getBoundPrimal();  // caches last child guide value
                while ( !beam.empty() ) {
                    NodePtr n = *beam.begin();
                    beam.erase(beam.begin());
                    if ( n.get()->isGoal() ) {
                        compareWithBest(n);
                    } else {  // no need to try to explore children
                        for ( auto c : n.get()->getChildren() ) {
                            if ( c.get()->isGoal() ) {
                                compareWithBest(c);
                                continue;
                            }
                            if ( static_cast<int>(children.size()) < k_ ) {
                                children.insert(c);
                                if ( static_cast<int>(children.size()) == k_ ) {
                                    least_children_guide = (*std::prev(children.end())).get()->guide();
                                }
                            } else {
                                double guide_c = c.get()->guide();
                                if ( manager_.isStrictlyBetter( guide_c, least_children_guide ) ) {
                                    remaining.insert(*std::prev(children.end()));
                                    children.erase(std::prev(children.end()));
                                    children.insert(c);
                                    least_children_guide = (*std::prev(children.end())).get()->guide();
                                } else {  // discard c
                                    remaining.insert(c);
                                }
                            }
                        }
                    }
                }
                // Children become the new beam
                beam.clear();
                beam.merge(children);
            }
        }

        // search ends. 
        if ( autorize_stop_ && remaining.empty() && beam.empty() ) {
            manager_.setEnd();  // sets end if the search depletes the tree
        }
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
