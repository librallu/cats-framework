#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

/**
 * \brief compares two nodes using g and h functions: f(n) := g(n) + w . h(n)
 */
struct CompareWAstar {
    double w_;
    explicit CompareWAstar(double w) : w_(w) {}

    bool operator()(const NodePtr a, const NodePtr b) const {
        return a.get()->evalPrefix()+w_*a.get()->evalSuffix() < b.get()->evalPrefix()+w_*b.get()->evalSuffix();
    }
};

/** \class WeightedAStar WeightedAStar.hpp
 *  \brief Implements a wA* tree search algorithm.
 */
class WeightedAStar : public TreeSearch {
 private:
    double w_;

 public:
 
    WeightedAStar(TreeSearchParameters params, double w)
        : TreeSearch(params), w_(w) {}

    std::string algorithmMessage() const override {
        return std::string("wA*(id:") + std::to_string(id_) + std::string(",") + std::to_string(w_) + std::string(")");
    }

    NodePtr run(double time_limit) override {
        // initialize fringe with root
        CompareWAstar compare(w_);
        std::multiset<NodePtr, CompareWAstar> fringe(compare);
        fringe.insert(root_);
        while ( !fringe.empty() && !manager_.searchEnded(time_limit) ) {
            NodePtr s = *fringe.begin();
            fringe.erase(fringe.begin());
            // lower bound cut
            if ( isDominatedByBest(s) ) continue;
            // if admissible, check if better than best sol
            if ( s.get()->isGoal() ) {
                compareWithBest(s);
                break;
            }
            for ( NodePtr t : s.get()->getChildren() ) {
                if ( isDominatedByBest(t) ) continue;
                fringe.insert(t);
            }
        }
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
