#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"
#include "../numeric.hpp"

namespace cats {

/**
 * \brief Implementation of a roulette wheel greedy random algorithm
 * Mostly used in ACO algorithms (see https://en.wikipedia.org/wiki/Fitness_proportionate_selection)
 */
class RouletteWheelGreedy : public TreeSearch {
 private:
    double q0_ = 0.;

 public:
    std::string algorithmMessage() const override {
        return std::string("GWheel(id:") + std::to_string(id_) + std::string(")");
    }

    explicit RouletteWheelGreedy(TreeSearchParameters params) : TreeSearch(params) {}

    NodePtr run(double time_limit) override {
        NodePtr current = root_;
        // std::cout << "a\n";
        while ( !isDominatedByBest(current) && !current.get()->isGoal() && !manager_.searchEnded(time_limit) ) {
            std::vector<NodePtr> children = current.get()->getChildren();
            if ( children.size() == 0 ) break;
            // perform a roulette wheel selection
            if ( randomFloat() < q0_ ) {
                NodePtr next = children[0];
                for ( NodePtr c : children ) {
                    if ( c.get()->guide() < next.get()->guide() ) {
                        next = c;
                    }
                }
                current.swap(next);
            } else {
                double val_max = 0;
                for ( NodePtr c : children ) {
                    val_max += 1./(c.get()->guide());
                    // val_max += c.get()->guide();
                }
                double limit_iteration = randomFloat()*val_max;
                double cpt = 0;
                for ( NodePtr c : children ) {
                    cpt += 1./(c.get()->guide());
                    // cpt += c.get()->guide();
                    if ( cpt >= limit_iteration ) {
                        current.swap(c);
                        break;
                    }
                }
            }
        }
        if (current.get()->isGoal()) compareWithBest(current);
        // std::cout << "b\n";
        this->terminate();
        return local_best_;
    }

    void set_q0(double q0) {
        q0_ = q0;
    }
};

}  // namespace cats
