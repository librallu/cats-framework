#pragma once

#include "../TreeSearch.hpp"
#include "../SolutionParts.hpp"
#include "RouletteWheelGreedy.hpp"
#include "GreedyRandom.hpp"
#include "../combinators/SolutionPartsCombinator.hpp"

namespace cats {


/**
 * \brief describes the type of ant colony optimization variants
 */
enum ACOType {
    AS,
    EAS,
    RAS,
    MMAS,
    ACS,
    ANTS,
    HyperCube,
    BeamACO,
};

/**
 * \brief describes a full parameter set of Ant Colony Optimization
 */
struct ACOParams {
    int nb_ants;
    double heuristic_weight;
    double pheromone_init;
    double pheromone_min;
    double pheromone_max;
    double evaporation_rate;
    double q0;
    double elitist_weight;
    int nb_elits;
    ACOType type;
};

/**
 * \brief describe the simple Ant System parameters 
 */
struct ASParams {
    int nb_ants;
    double heuristic_weight;
    double evaporation_rate;
};

struct MMASParams {
    double pheromone_min;
    double pheromone_max;
};

/**
 * \brief Implements an Ant Colony Optimization algorithm
 */
template <typename T, typename SP>
class ACO : public TreeSearch {
 private:
    SolutionPartsStore<SP>& store_;
    ACOParams aco_params_;
    TreeSearchParameters ts_params_;
    std::vector<NodePtr> iteration_solutions_;

 public:
    std::string algorithmMessage() const override {
        return std::string("ACO(id:") + std::to_string(id_) + std::string(")");
    }

    ACO(TreeSearchParameters ts_params, SolutionPartsStore<SP>& store)
        : TreeSearch(ts_params), store_(store), ts_params_(ts_params) {}

    void set_as(ASParams params, double pheromone_init = 1.) {
        aco_params_.nb_ants = params.nb_ants;
        aco_params_.heuristic_weight = params.heuristic_weight;
        aco_params_.evaporation_rate = params.evaporation_rate;
        aco_params_.pheromone_init = pheromone_init;
        aco_params_.type = AS;
    }

    void set_eas(ASParams params, double elitist_weight = 1., double pheromone_init = 1.) {
        aco_params_.nb_ants = params.nb_ants;
        aco_params_.pheromone_init = pheromone_init;
        aco_params_.heuristic_weight = params.heuristic_weight;
        aco_params_.evaporation_rate = params.evaporation_rate;
        aco_params_.elitist_weight = elitist_weight;
        aco_params_.type = EAS;
    }

    void set_ras(ASParams params, int nb_elits, double pheromone_init = 1.) {
        aco_params_.nb_ants = params.nb_ants;
        aco_params_.pheromone_init = pheromone_init;
        aco_params_.heuristic_weight = params.heuristic_weight;
        aco_params_.evaporation_rate = params.evaporation_rate;
        aco_params_.elitist_weight = nb_elits;
        aco_params_.nb_elits = nb_elits;
        aco_params_.type = RAS;
    }

    void set_mmas(ASParams params, MMASParams mmas_params) {
        aco_params_.nb_ants = params.nb_ants;
        aco_params_.heuristic_weight = params.heuristic_weight;
        aco_params_.evaporation_rate = params.evaporation_rate;
        aco_params_.pheromone_min = mmas_params.pheromone_min;
        aco_params_.pheromone_max = mmas_params.pheromone_max;
        aco_params_.type = MMAS;
    }

    void set_acs(ASParams params, double q0) {
        aco_params_.nb_ants = params.nb_ants;
        aco_params_.heuristic_weight = params.heuristic_weight;
        aco_params_.evaporation_rate = params.evaporation_rate;
        aco_params_.q0 = q0;
        aco_params_.type = ACS;
    }

    NodePtr run(double time_limit) {
        // initialize pheromones
        initializePheromones();
        initializeTSParams();
        // while stopping criterion not reached
        int nb_iter = 0;
        while ( !manager_.searchEnded(time_limit) ) {
            std::cout << "====\titeration " << nb_iter++ << "\t====\n";
            // std::cout << "== START ACO ITERATION" << std::endl;
            // run nbAnts
            for ( int i = 0 ; i < aco_params_.nb_ants ; i++ ) {
                this->runTreeSearch(time_limit);
            }
            // update pheromones
            // std::cout << "== UPDATE PHEROMONES" << std::endl;
            this->updatePheromones();
            // empty iteration_solutions
            // std::cout << "empty iteration solutions list\n";
            iteration_solutions_.clear();
        }
        this->terminate();
        return local_best_;
    }

    void initializePheromones() {
        switch ( aco_params_.type ) {
            case AS:
            case EAS:
            case RAS:
            case ACS:
                for ( SP sp : dynamic_cast<SolutionPartsNode<SP>*>(root_.get())->enumerateSolutionParts() ) {
                    store_.set(sp, { .pheromones = aco_params_.pheromone_init });
                }
            break;
            case MMAS:
                std::cout << aco_params_.pheromone_max <<std::endl;
                for ( SP sp : dynamic_cast<SolutionPartsNode<SP>*>(root_.get())->enumerateSolutionParts() ) {
                    store_.set(sp, { .pheromones = aco_params_.pheromone_max });
                }
            break;
            default:
                std::cout << "ACO VARIANT NOT IMPLEMENTED" << std::endl;
                return;
            break;
        }
    }

    void initializeTSParams() {
        ts_params_.root = NodePtr(new SolutionPartsCombinator<T, SP>(root_, store_, aco_params_.heuristic_weight));
        ts_params_.root.get()->set_enable_cuts(false);
    }

    void runTreeSearch(double time_limit) {
        switch ( aco_params_.type ) {
            case AS:
            case EAS:
            case RAS:
            case MMAS:
            case ACS:
                RouletteWheelGreedy ts = RouletteWheelGreedy(ts_params_);
                // GreedyRandom ts = GreedyRandom(ts_params_);
                // ts.doOnlyOneIteration(true);
                ts.set_check_dominance(false);
                if ( aco_params_.type == ACS ) {  // if ACS, register
                    ts.set_q0(aco_params_.q0);
                }
                NodePtr res = ts.run(time_limit);
                if ( res.get() != nullptr ) {
                    compareWithBest(res);
                    iteration_solutions_.push_back(res);
                }
            break;
        }
    }

    void updatePheromones() {
        SolutionPartsNode<SP>* r = dynamic_cast<SolutionPartsNode<SP>*>(root_.get());
        if ( aco_params_.type == AS || aco_params_.type == EAS || aco_params_.type == RAS ) {
            // apply pheromone evaporation
            for ( SP sp : r->enumerateSolutionParts() ) {
                SolutionPartInfos e = store_.get(sp);
                e.pheromones = (1-aco_params_.evaporation_rate)*e.pheromones;
                store_.set(sp, e);
            }
            // each ant deposits pheromones
            if ( aco_params_.type == AS || aco_params_.type == EAS ) {
                for ( NodePtr s : iteration_solutions_ ) {
                    addSolutionToPheromones(s);
                }
            } else {  // If rank ant system
                // sort ants
                std::sort(iteration_solutions_.begin(), iteration_solutions_.end(), [](const NodePtr& a, const NodePtr& b) {
                    return a.get()->evaluate() < b.get()->evaluate();
                });
                for ( int i = 0 ; i < std::min(static_cast<int>(iteration_solutions_.size()), aco_params_.nb_elits-1) ; i++ ) {
                    addSolutionToPheromones(iteration_solutions_[i], aco_params_.nb_elits-i);
                }
            }
            if ( aco_params_.type == EAS ) {
                addSolutionToPheromones(manager_.getBest(), aco_params_.elitist_weight);
            }
        } else if ( aco_params_.type == MMAS ) {
            // apply pheromone evaporation
            for ( SP sp : r->enumerateSolutionParts() ) {
                SolutionPartInfos e = store_.get(sp);
                e.pheromones = std::max((1-aco_params_.evaporation_rate)*e.pheromones, aco_params_.pheromone_min);
                store_.set(sp, e);
            }
            // add pheromones depending on solutions found during last iteration
            if ( randomFloat() < 0.5 ) {  // one in a while, add best solution found to the pheromones
                if ( manager_.getBest() != nullptr ) {
                    addSolutionToPheromones(manager_.getBest());
                }
            } else {
                if ( !iteration_solutions_.empty() ) {  // add best solution of last iteration
                    addSolutionToPheromones(getBestFromIteration());
                }
            }
        } else if ( aco_params_.type == ACS ) {
            // evaporation only on path choosen by current solutions
            for ( NodePtr s : iteration_solutions_ ) {
                for ( SP sp : dynamic_cast<SolutionPartsNode<SP>*>(s.get())->getAllSolutionParts() ) {
                    SolutionPartInfos e = store_.get(sp);
                    e.pheromones = (1-aco_params_.evaporation_rate)*e.pheromones;
                    store_.set(sp, e);
                }
            }
            if ( !iteration_solutions_.empty() ) {  // only best deposits pheromones
                addSolutionToPheromones(getBestFromIteration());
            }
        } else {
            std::cout << "ACO VARIANT NOT IMPLEMENTED" << std::endl;
            return;
        }
    }

    NodePtr getBestFromIteration() const {
        NodePtr best_iter = iteration_solutions_[0];
        for ( int i = 1 ; i < static_cast<int>(iteration_solutions_.size()) ; i++ ) {
            if ( iteration_solutions_[i].get()->evaluate() < best_iter.get()->evaluate() ) {
                best_iter = iteration_solutions_[i];
            }
        }
        return best_iter;
    }

    void addSolutionToPheromones(NodePtr s, double weight = 1) {
        SolutionPartsNode<SP>* sol_pointer = dynamic_cast<SolutionPartsNode<SP>*>(s.get());
        // std::cout << sol_pointer->evaluate() << std::endl;
        for ( SP sp : sol_pointer->getAllSolutionParts() ) {
            // std::cout << sp << std::endl;
            SolutionPartInfos e = store_.get(sp);
            double C = weight;
            double d_tau = C / sol_pointer->evaluate();
            if ( aco_params_.type == AS || aco_params_.type == EAS || aco_params_.type == RAS || aco_params_.type == ACS ) {
                e.pheromones += d_tau;
                store_.set(sp, e);
            } else if ( aco_params_.type == MMAS ) {
                e.pheromones = std::min(e.pheromones+d_tau/sol_pointer->evaluate(), aco_params_.pheromone_max);
                store_.set(sp, e);
            } else {
                std::cout << "ACO VARIANT NOT IMPLEMENTED" << std::endl;
            }
        }
    }
};

}  // namespace cats
