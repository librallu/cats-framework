#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>


#include "BeamSearch.hpp"
#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \class IterativeBeamSearch
 * \brief Implements an iterative beam search algorithm.
 * It runs several beam searches with an initial beam size of *d* and multiplies it by *gf* at every iteration.
 */
class IterativeBeamSearch : public TreeSearch {
 private:
    int d_;     //initial beam size
    float gf_;  //size multiplication factor
    std::string msg_;
    bool autorize_end_;
    bool assume_nondecreasing_guide_;
    bool assume_sorted_children_;

 public:

    IterativeBeamSearch(TreeSearchParameters params, int d, float gf, bool autorize_end = true):
      TreeSearch(params), d_(d), gf_(gf), msg_("no run yet"), autorize_end_(autorize_end), assume_nondecreasing_guide_(false), assume_sorted_children_(false) {}

    std::string algorithmMessage() const override {
        return msg_;
    }

    void assumeNondecreasingGuide(bool v = true) {
        assume_nondecreasing_guide_ = v;
    }

    void assumeSortedChildren(bool v = true) {
        assume_sorted_children_ = v;
    }

    NodePtr run(double time_limit) override {
        // time checking
        double d = d_;
        while ( !manager_.searchEnded(time_limit) && d > 0 ) {
            BeamSearch algorithm({.root = root_, .manager = manager_, .id = id_}, d, autorize_end_);
            algorithm.set_check_dominance(do_check_dominance_);
            algorithm.set_measure_heuristic_cuts(measure_heuristic_cuts_);
            algorithm.assumeNondecreasingGuide(assume_nondecreasing_guide_);
            algorithm.assumeSortedChildren(assume_sorted_children_);
            msg_ = algorithm.algorithmMessage();
            root_.get()->setTreeSearchName(&msg_);
            NodePtr best_alg = algorithm.run(time_limit);
            root_.get()->newIter();
            if ( best_alg != nullptr && best_alg.get() != nullptr ) {
                if ( local_best_.get() == nullptr || best_alg.get()->evaluate() < local_best_.get()->evaluate() ) {
                    local_best_ = best_alg;
                }
            }
            d *= gf_;
        }
        return local_best_;
    }
};

}  // namespace cats
