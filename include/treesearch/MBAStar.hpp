#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <string>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

/**
 * \class MBAStar
 * \brief Implements a Memory Bounded A* algorithm. It is similar to a Beam Search but uses a best first principle instead
 */
class MBAStar : public TreeSearch {
 private:
    int d_;
    bool autorize_stop_;

 public:

    MBAStar(TreeSearchParameters params, int d, bool autorize_stop = true):
      TreeSearch(params), d_(d), autorize_stop_(autorize_stop) {}

    std::string algorithmMessage() const override {
        return std::string("MBA*(id:") + std::to_string(id_) + std::string(",") + std::to_string(d_) + std::string(")");
    }

    NodePtr run(double time_limit) override {
        std::string msg = algorithmMessage();
        root_.get()->setTreeSearchName(&msg);
        // initialize fringe with root
        bool heuristic_cut_done = false;
        std::multiset<NodePtr, Compare> fringe(compare_);
        fringe.insert(root_);
        while ( !fringe.empty() && !manager_.searchEnded(time_limit) ) {
            NodePtr s = *fringe.begin();
            fringe.erase(fringe.begin());
            // if admissible, check if better than best sol
            if ( s.get()->isGoal() ) {
                compareWithBest(s);
                continue;
            }
            for ( NodePtr t : s.get()->getChildren() ) {
                if ( t.get()->isGoal() ) {
                    compareWithBest(t);
                    continue;
                }
                // check if not too big before adding elt in fringe
                if ( static_cast<int>(fringe.size()) < d_ || compare_(t, *(std::prev(fringe.end()))) ) {
                    fringe.insert(t);
                    if ( static_cast<int>(fringe.size()) > d_ ) {
                        fringe.erase(std::prev(fringe.end()));
                        heuristic_cut_done = true;
                    }
                }
                assert(static_cast<int>(fringe.size()) <= d_);
            }
        }
        if ( autorize_stop_ && !manager_.searchEnded(time_limit) && !heuristic_cut_done ) {
            manager_.setEnd();  // no heuristic cut, proven the optimal
        }
        this->terminate();
        return local_best_;
    }

};

}  // namespace cats
