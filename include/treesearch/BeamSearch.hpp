#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {


/**
 * \class BeamSearch
 * \brief Implements a beam search algorithm. More information here: https://en.wikipedia.org/wiki/Beam_search
 */
class BeamSearch :public TreeSearch {
 private:
    int d_;
    bool autorize_stop_;
    std::string msg_;
    bool assume_nondecreasing_guide_;
    bool assume_sorted_children_;

 public:

    BeamSearch(TreeSearchParameters params, int d, bool autorize_stop = false):
      TreeSearch(params), d_(d), autorize_stop_(autorize_stop), assume_nondecreasing_guide_(false), assume_sorted_children_(false) {
          set_msg();
      }

    void set_msg(std::string msg) {
        msg_ = msg;
    }

    void assumeNondecreasingGuide(bool v = true) {
        assume_nondecreasing_guide_ = v;
    }

    void assumeSortedChildren(bool v = true) {
        assume_sorted_children_ = v;
    }

    void set_msg() {
        msg_ = std::string("BS(id:") + std::to_string(id_) + std::string(",") + std::to_string(d_) + std::string(")");
    }

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        // initialize beam with root
        if ( d_ < 0 ) return local_best_;
        std::multiset<NodePtr, Compare> original_beam(compare_);
        std::multiset<NodePtr, Compare> beam(compare_);
        root_.get()->setTreeSearchName(&msg_);
        beam.insert(root_);
        bool heuristic_cut_done = false;
        while ( !beam.empty() && !manager_.searchEnded(time_limit) ) {  // for each level of the tree
            // Beam Iteration: compute list of child nodes from the current level
            std::multiset<NodePtr, Compare> children(compare_);
            double least_children_guide = manager_.getBoundPrimal();  // caches last child guide value
            while ( !beam.empty() ) {
                NodePtr n = *beam.begin();
                beam.erase(beam.begin());
                if ( n.get()->isGoal() ) {
                    compareWithBest(n);
                } else {  // no need to try to explore children
                    if ( assume_nondecreasing_guide_ && static_cast<int>(children.size()) >= d_ 
                    && !manager_.isBetter( n.get()->guide(), least_children_guide ) ) {
                        heuristic_cut_done = true;
                        continue;
                    }
                    for ( auto c : n.get()->getChildren() ) {
                        if ( c.get()->isGoal() ) {
                            compareWithBest(c);
                            continue;
                        }
                        if ( static_cast<int>(children.size()) < d_ ) {
                            children.insert(c);
                            if ( static_cast<int>(children.size()) == d_ ) {
                                least_children_guide = (*std::prev(children.end())).get()->guide();
                            }
                        } else {
                            heuristic_cut_done = true;
                            double guide_c = c.get()->guide();
                            if ( manager_.isStrictlyBetter( guide_c, least_children_guide ) ) {
                                children.erase(std::prev(children.end()));
                                children.insert(c);
                                least_children_guide = (*std::prev(children.end())).get()->guide();
                            } else if ( assume_sorted_children_ ) { break; }
                        }
                    }
                }
            }
            // Children become the new beam
            beam.clear();
            beam.merge(children);
        }
        if ( autorize_stop_ && !manager_.searchEnded(time_limit) && !heuristic_cut_done ) {
            manager_.setEnd();  // no heuristic cut, proven the optimal
        }
        this->terminate();
        return local_best_;
    }

};

}  // namespace cats
