#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>

#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

struct NodePriority {
    NodePtr n;
    double f;
};

struct PriorityCompare {
    SearchManager& manager_;

    explicit PriorityCompare(SearchManager& manager): manager_(manager) {}

    bool operator()(const NodePriority& a, const NodePriority& b) {
        return a.f < b.f;
    }
};

/**
 * \class BeamSearch2
 * \brief Implements a beam search algorithm. More information here: https://en.wikipedia.org/wiki/Beam_search
 * Exploits the lazy children generation. Good for branching schemes with large branching factors
 */
class BeamSearch2 :public TreeSearch {
 private:
    int d_;
    bool autorize_stop_;
    std::string msg_;
    bool assume_nondecreasing_guide_;
    bool assume_sorted_children_;

 public:

    BeamSearch2(TreeSearchParameters params, int d, bool autorize_stop = false):
      TreeSearch(params), d_(d), autorize_stop_(autorize_stop), assume_nondecreasing_guide_(false), assume_sorted_children_(false) {
          set_msg();
      }

    void set_msg(std::string msg) {
        msg_ = msg;
    }

    void assumeNondecreasingGuide(bool v = true) {
        assume_nondecreasing_guide_ = v;
    }

    void assumeSortedChildren(bool v = true) {
        assume_sorted_children_ = v;
    }

    void set_msg() {
        msg_ = std::string("BSlc(id:") + std::to_string(id_) + std::string(",") + std::to_string(d_) + std::string(")");
    }

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        // initialize beam with root
        if ( d_ < 0 ) return local_best_;
        std::multiset<NodePtr, Compare> beam(compare_);
        root_.get()->setTreeSearchName(&msg_);
        beam.insert(root_);
        bool heuristic_cut_done = false;
        while ( !beam.empty() && !manager_.searchEnded(time_limit) ) {  // for each level of the tree
            // Beam Iteration: compute list of child nodes from the current level
            PriorityCompare priority_compare(manager_);
            std::multiset<NodePriority, PriorityCompare> beam_tmp(priority_compare);
            std::multiset<NodePtr, Compare> children(compare_);
            for ( NodePtr n : beam ) {
                beam_tmp.insert({.n = n, .f = assume_nondecreasing_guide_ ? n.get()->guide() : 0 });
            }
            while ( !beam_tmp.empty() ) {
                NodePriority s = *beam_tmp.begin();
                beam_tmp.erase(beam_tmp.begin());
                if ( s.n.get()->isGoal() ) {
                    compareWithBest(s.n);
                }
                if ( s.n.get()->hasNextChild() ) {
                    NodePtr t = s.n.get()->nextChild();
                    if ( static_cast<int>(children.size()) < d_ ) {
                        children.insert(t);
                        beam_tmp.insert({ .n = s.n, .f = t.get()->guide() });
                    } else {
                        heuristic_cut_done = true;
                        if ( !assume_sorted_children_ || manager_.isStrictlyBetter(t.get()->guide(), (*std::prev(children.end())).get()->guide()) ) {
                            children.insert(t);
                            children.erase(std::prev(children.end()));
                            beam_tmp.insert({ .n = s.n, .f = t.get()->guide() });
                        }
                    }
                }
            }
            // Children become the new beam
            beam.clear();
            beam.merge(children);       
        }
        if ( autorize_stop_ && !manager_.searchEnded(time_limit) && !heuristic_cut_done ) {
            manager_.setEnd();  // no heuristic cut, proven the optimal
        }
        this->terminate();
        return local_best_;
    }

};

}  // namespace cats
