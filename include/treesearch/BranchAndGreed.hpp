#pragma once

#include <vector>
#include <set>
#include <iostream>
#include <omp.h>


#include "Greedy.hpp"
#include "../combinators/ProbingCombinator.hpp"
#include "../Node.hpp"
#include "../SearchManager.hpp"
#include "../TreeSearch.hpp"

namespace cats {

/**
 * \class BranchAndGreed
 * \brief Implements a BranchAndGreed algorithm. It runs a greedy algorithm guided by an other greedy run at each child.
 */
template <typename T = int, typename U = int>
class BranchAndGreed : public TreeSearch {
 private:
    std::string msg_;
    double alpha_;

    void constructMessage(int d) {
        msg_ = std::string("BranchGreed(id:") + std::to_string(id_) + std::string(")");
    }

 public:

    explicit BranchAndGreed(TreeSearchParameters params, double alpha):
      TreeSearch(params), msg_("no run yet"), alpha_(alpha) {
        constructMessage(params.id);
      }

    std::string algorithmMessage() const override {
        return msg_;
    }

    NodePtr run(double time_limit) override {
        TreeSearchParameters params = {.root = root_, .manager = manager_, .id = id_};
        NodePtr root = NodePtr(new ProbingCombinator<T, U>(root_, manager_, alpha_, params, time_limit, [](TreeSearchParameters p, double tl) {
          Greedy ts = Greedy(p);
          ts.set_check_dominance(false);  // disable dominance check
          return ts.run(tl);
        }));
        params.root = root;
        Greedy greedy = Greedy(params);
        local_best_ = greedy.run(time_limit);
        this->terminate();
        return local_best_;
    }
};

}  // namespace cats
