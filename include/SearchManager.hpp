#pragma once

#include <string>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <vector>
#include <cassert>
#include <mutex>
#include <cmath>

#include "SearchStats.hpp"
#include "Node.hpp"
#include "io.hpp"
#include "numeric.hpp"

enum OptimSense { MIN, MAX };

namespace cats {

class Node;
typedef std::shared_ptr<Node> NodePtr;

/**
 * \brief provides methods to store information about the search and inform search algorithms if the search ended *etc.*
 */
class SearchManager {
 private:
    // GLOBAL OPTIM VARIABLES
    bool ended_flag_;   ///< set to true when we want to stop the search
    int nb_sol_;   ///< number of solutions found so far
    std::mutex lock_;   ///< lock for updating best solution
    NodePtr best_ptr_;  ///< best solution pointer
    double best_primal_;   ///< best solution value
    bool init_best_primal_;  ///< true if a primal was given
    OptimSense optim_sense_;   ///< is minimization or maximization
    std::chrono::system_clock::time_point start_time_;   ///< time when the search started

    // DISPLAY VARIABLES
    bool verbose_;
    std::vector<int> col_sizes_;  ///< sizes of columns
    std::vector<std::string> title_;   ///< text contained in title

    // SEARCH STATISTICS
    SearchStats stats_;
    bool stats_registered_ = false;

 public:
    explicit SearchManager(OptimSense type = OptimSense::MIN, bool verbose=true): ended_flag_(false), nb_sol_(0), init_best_primal_(false),
    optim_sense_(type), verbose_(verbose) {
        col_sizes_ = {6, 10, 15, 15, 15, 30, 25};
        title_ = {"sol nb", "time", "nb nodes", "nodes/s", "objective", "algorithm", "branching scheme"};
    }

    SearchStats& getSearchStats() {
        stats_registered_ = true;
        return stats_;
    }

    /**
     * \brief returns the optimization sense (Minimization or a Maximization)
     */
    OptimSense get_optim_sense() {
        return optim_sense_;
    }

    /**
     * \brief returns true if a is strictly better than b
     */
    bool isStrictlyBetter(double a, double b) const {
        if ( optim_sense_ == OptimSense::MIN ) {
            return a < b;
        } else {
            return a > b;
        }
    }

    /**
     * \brief if v is set to false, the manager does not display anything during the search.
     */
    void set_verbose(bool v) { verbose_ = v; }

    /**
     * \brief if v is set to false, the manager does not display anything during the search.
     */
    void set_best_primal(double p) { init_best_primal_ = true; best_primal_ = p; }

    /**
     * \brief returns true if a is better or equal than b
     */
    bool isBetter(double a, double b) const {
        if ( optim_sense_ == OptimSense::MIN ) {
            return a <= b;
        } else {
            return a >= b;
        }
    }

    /**
     * \brief returns the best among a and b (min(a,b) or max(a,b))
     */
    double bestAmong(double a, double b) const {
        return isBetter(a, b) ? a : b;
    }

    /**
     * \brief called when the search ends. Inform search manager that search ended.
     */
    inline bool searchEnded(double time_limit) {
        double elapsedTime = getElapsedTime();
        bool searchEnded = elapsedTime > time_limit || ended_flag_;
        return searchEnded;
    }

    /**
     * \brief end search
     */
    inline void setEnd() { ended_flag_ = true; }

    /**
     * \brief get title of log columns.
     */
    std::string getTitle(Color c = Green) {
        std::stringstream res;
        res << std::right;
        assert(title_.size() == col_sizes_.size());
        for ( int i = 0 ; i < static_cast<int>(title_.size()) ; i++ ) {
            res << std::setw(col_sizes_[i]) << title_[i];
        }
        return stringColor(c, res.str());
    }

    /**
     * \brief get formated information.
     */
    std::string getInfo(std::vector<std::string> infos) {
        std::stringstream res;
        res << std::right;
        assert(infos.size() == col_sizes_.size());
        for ( int i = 0 ; i < static_cast<int>(infos.size()) ; i++ ) {
            res << std::setw(col_sizes_[i]) << infos[i];
        }
        return res.str();
    }

    /**
     * \brief get formated information using standard display
     */
    std::string getInfo(std::string algoname, int id) {
        std::string eval = "-";
        if ( best_ptr_ != nullptr ) {
            eval = dotNumber(best_primal_);
        }
        return getInfo({
            std::to_string(nb_sol_),
            getDoubleAtPrecision(getElapsedTime(), 3),
            printBigInt(getNbGenerated()),
            printBigInt(getNbGenerated()/getElapsedTime()),
            eval,
            algoname,
            best_ptr_.get()->getName()
        });
    }

    /**
     * \brief returns the value of the best soulution found or infinity if none found
     */
    inline double getPrimal() const {
        return best_primal_;
    }

    /**
     * \brief returns bound on primal values (infinity or -infinity depending on the optimization direction)
     */
    double getBoundPrimal() const {
        return ( optim_sense_ == MIN ) ? MAX_DOUBLE : MIN_DOUBLE;
    }

    /**
     * \brief returns number of solutions found
     */
    int getNbSol() const { return nb_sol_; }

    /**
     * \brief Initializes the start time.
     */
    inline void startTimeMeasure() {
        start_time_ = std::chrono::system_clock::now();
    }

    /**
     * \brief get time spent during the search.
     */
    inline double getElapsedTime() const {
      std::chrono::duration<double> res = std::chrono::system_clock::now() - start_time_;
      return res.count();
    }

    /**
     * \brief return the total of number of nodes computed so far by all registered algorithms
     */
    inline NodeNumber getNbGenerated() const {
      return stats_.get_nb_generated();
    }

    /**
     * \brief returns the number of nodes per second generated
     */
    inline NodeNumber getNbNodesPerSecond() const {
        return static_cast<double>(getNbGenerated())/getElapsedTime();
    }


    /**
     * \brief print some statistics of the search (nb of opened nodes, search time, *etc.*)
     */
    void printStats() const {
        if ( !verbose_ ) return;
        if ( stats_registered_ ) {
            std::cout << title1String("Search statistics", 40, "=") << std::endl;
            stats_.printStats();
            std::cout << "searched\t\t" << getElapsedTime() << " s\n";
            std::cout << "generated nodes / s\t" << dotNumber(getNbNodesPerSecond()) << std::endl;
            if ( nb_sol_ == 0 ) {
                std::cout << "No solution found" << std::endl;
            } else {
                double primal = best_primal_;
                std::cout << "Primal\t\t\t" << dotNumber(primal) << std::endl;
            }
        } else {
            std::cout << stringColor(Orange, "No Search statistics recorded") << "\n\n";
        }
    }

    /**
     * \brief Start manager and optionnaly print title.
     */
    void start() {
      startTimeMeasure();
      nb_sol_ = 0;
      if ( verbose_ ) {
        std::cout << "\n" << getTitle() << "\n\n";
      }
      if (!init_best_primal_){ init_best_primal_ = true; best_primal_ = getBoundPrimal(); }  // initializes best sol value
    }

    /**
     * \brief returns true if current sol dominated by best or below acceptance threshold
     */
    bool isDominatedByBest(NodePtr s, float acceptance_threshold = 0.) {
        if ( best_ptr_ == nullptr ) return false;
        double eval_s = s.get()->evaluate();
        double eval_best = best_primal_;
        if (optim_sense_ == OptimSense::MIN)
            return eval_s >= (1.+acceptance_threshold)*eval_best;
        else  // if MAXIMIZATION problem
            return (1.+acceptance_threshold)*eval_s <= eval_best;
    }

    /**
     * \brief compares new solution with best, update best if needed
     */
    void compareWithBest(NodePtr s, std::string algo_msg, int id) {
        double eval_s = s.get()->evaluate();
        if ( s.get()->isGoal() ) {
            lock_.lock();
            bool local_best_exists = !(best_ptr_ == nullptr);
            bool comparison = false;
            if (local_best_exists) {
                comparison = isStrictlyBetter(eval_s, best_primal_);
            }
            if (!local_best_exists || comparison) {
                best_ptr_.swap(s);
                best_primal_ = eval_s;
                nb_sol_++;
                best_ptr_.get()->handleNewBest();
                if ( verbose_ ) std::cout << getInfo(algo_msg, id) << std::endl;
            }
            lock_.unlock();
        }
    }

    /**
     * \brief returns best solution found so far
     */
    NodePtr getBest() { return best_ptr_; }

    /**
     * \brief writes anytime curve to a file using the SearchStats registered anytime curve
     */
    void writeAnytimeCurve(std::string filename, std::string title, bool addFacticePoint=true) {
        if ( addFacticePoint ) stats_.addFacticeObj(getElapsedTime());
        AnytimeCurve c = stats_.getAnytimeCurve();
        std::ofstream f;
        f.open(filename);
        f << c.toJson(title);
        f.close();
    }

    /**
     * \brief writes best order result in file
     */
    void writeSolution(std::string filename) {
        // write solution file
        best_ptr_->writeSol(filename);
    }

    /**
     * \brief writes node opening events in a csv file
     */
    void writeNodeOpeningEvents(std::string filename) {
        std::ofstream f;
        f.open(filename);
        f << "id;parentid;g;h;guide;nodemsg;arcmsg;goal\n";
        for ( NodeOpeningEvent e : stats_.getNodeEvents() ) {
            f << e.nodeid << ";"
              << e.parent << ";"
              << e.g << ";"
              << e.h << ";"
              << e.guide << ";"
              << e.nodemsg << ";"
              << e.arcmsg << ";"
              << e.goal << "\n";
        }
        f.close();
    }
};


}  // namespace cats
