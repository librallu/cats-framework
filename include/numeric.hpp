#pragma once

#include <random>
#include <limits>
#include <cmath>

namespace cats {

///< uniform random float between 0 and 1
inline const float randomFloat() { return static_cast<float>( rand()) / RAND_MAX; }  

///< uniform random float between a and b
inline const float randomFloat(float a, float b) { return (b-a)*randomFloat()+a; }

///< random int between [mini ; maxi[
inline const float randomInt(int mini, int maxi) { return static_cast<int>(randomFloat()*(maxi-mini)+mini); }

///< abs(x)
inline const double absDouble(double a) { return a < 0 ? -a : a; }

/**
 * \brief returns a^b
 */
inline const double fastPow(double a, int b) {
    if ( b == 0 ) return 1;
    if ( b == 1 ) return a;
    double res = fastPow(a, b/2);
    if ( b%2 == 0 ) return res*res;
    return a*res*res;
}

const double MAX_DOUBLE = std::numeric_limits<double>::max();   ///< big number used as maximum double
const double MIN_DOUBLE = std::numeric_limits<double>::min();   ///< big number used as minimum double

}  // namespace cats
