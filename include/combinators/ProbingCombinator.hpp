#pragma once

#include <functional>

#include "../SearchManager.hpp"
#include "../Combinator.hpp"
#include "../Node.hpp"
#include "../TreeSearch.hpp"
#include "../treesearch/Greedy.hpp"
#include "../numeric.hpp"

namespace cats {

typedef std::function<NodePtr(TreeSearchParameters, double)> TreeSearchFactory;

/** \class ProbingCombinator
 * \brief combinator that modifies the guide according to the evaluation of a greedy-like algorithm
 * if alpha = 0 : use only lower bound, if alpha = 1, use only upper bound
 */
template <typename T = int, typename SP = int>
class ProbingCombinator : public Combinator<T, SP> {
 private:
    double alpha_;
    TreeSearchParameters params_;
    double time_limit_;
    double ub_ = MAX_DOUBLE;
    std::string* algo_msg_;
    TreeSearchFactory greedy_;

 public:
    ProbingCombinator(NodePtr node, SearchManager& manager, double alpha, TreeSearchParameters params, double time_limit,
    TreeSearchFactory greedy, std::string* algo_msg = nullptr) :
        Combinator<T, SP>(node, manager), alpha_(alpha), params_(params), time_limit_(time_limit),
        algo_msg_(algo_msg), greedy_(greedy) {}

    ProbingCombinator(const ProbingCombinator& n) :
        Combinator<T, SP>(n), alpha_(n.alpha_), params_(n.params_), time_limit_(n.time_limit_), algo_msg_(n.algo_msg_), greedy_(n.greedy_) {}

    /******* IMPLEMENTATION OF BRANCHING SCHEME INTERFACE *******/
    NodePtr copy() const override { return NodePtr(new ProbingCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for ( NodePtr a : this->node_.get()->getChildren() ) {
            res.push_back(NodePtr(new ProbingCombinator(a, this->manager_, alpha_, params_, time_limit_, greedy_, algo_msg_)));
        }
        return res;
    }

    void setAlpha(double alpha) {
        alpha_ = alpha;
    }

    double guide() override {
        // std::cout << "START PROBING\n";
        double lb = this->node_.get()->evaluate();
        bool perform_probing = true;
        if ( alpha_ < 0.0001 ) {
            perform_probing = false;
        }
        if ( ub_ >= MAX_DOUBLE && perform_probing ) {
            if ( perform_probing ) {
                TreeSearchParameters greedy_params = {
                    .root = this->node_.get()->copy(),
                    .manager = params_.manager,
                    .id = params_.id
                };
                NodePtr res_greedy = greedy_(greedy_params, time_limit_);
                if ( res_greedy != nullptr && res_greedy.get() != nullptr ) {
                    ub_ = res_greedy.get()->evaluate();
                }
            }
        }
        if ( alpha_ < 0.0001 ) {
            ub_ = 0;
        }
        return (1.-alpha_)*lb + alpha_*ub_;
    }

    std::string getName() override {
        return std::string("P(")+this->node_.get()->getName()+std::string(")");
    }

    void setTreeSearchName(std::string* alg_name) override {
        algo_msg_ = alg_name;
        this->node_.get()->setTreeSearchName(alg_name);
    }
};

}  // namespace cats
