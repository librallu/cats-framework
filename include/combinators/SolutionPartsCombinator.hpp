#pragma once

#include "../SearchManager.hpp"
#include "../Combinator.hpp"
#include "../numeric.hpp"
#include "../SolutionParts.hpp"

namespace cats {


enum SolPartCombinatorType {
    Pheromones,
    UCT
};

/** \class SolutionPartsCombinator
 * \brief combinator that modifies the guide function to integrate solution parts informations (mostly pheromones)
 */
template <typename T = int, typename SP = int>
class SolutionPartsCombinator : public Combinator<T, SP> {
 private:
    SolutionPartsStore<SP>& store_;
    int heuristic_weight_;
    SolPartCombinatorType type_;

 public:
    SolutionPartsCombinator(NodePtr node, SolutionPartsStore<SP>& store, int heuristic_weight = 1, SolPartCombinatorType type = Pheromones) :
        Combinator<T, SP>(node), store_(store), heuristic_weight_(heuristic_weight), type_(type) {}

    SolutionPartsCombinator(const SolutionPartsCombinator& n) :
        Combinator<T, SP>(n), store_(n.store_), heuristic_weight_(n.heuristic_weight_), type_(n.type_) {}

    /******* IMPLEMENTATION OF BRANCHING SCHEME INTERFACE *******/
    NodePtr copy() const override { return NodePtr(new SolutionPartsCombinator(*this)); }

    std::vector<NodePtr> getChildren(double& smallest_heuristic_cut) override {
        std::vector<NodePtr> res;
        for ( NodePtr a : this->node_.get()->getChildren(smallest_heuristic_cut) ) {
            res.push_back(NodePtr(new SolutionPartsCombinator<T, SP>(a, store_, heuristic_weight_, type_)));
        }
        return res;
    }

    /**
     * \brief modifies guide to integrate some pheromones
     */
    double guide() override {
        // std::cout << "===\n";
        double res = fastPow(dynamic_cast<SolutionPartsNode<SP>*>(this->node_.get())->guideSP(), heuristic_weight_);
        double heuristic_value = res;
        for ( SP sp : this->getLastSolutionPart() ) {
            assert(store_.contains(sp));
            if ( type_ == Pheromones ) {
                double pheromone_value = fastPow(store_.get(sp).pheromones, this->getSolutionPartWeight(this->getSolPartId(sp)));
                // std::cout << pheromone_value << std::endl;
                res /= fastPow(pheromone_value, this->getSolutionPartWeight(this->getSolPartId(sp)));
            }
        }
        // std::cout << heuristic_value << "\t" << res << std::endl;
        // std::cout << res << std::endl;
        return res;
    }

    std::string getName() override {
        return std::string("SP(")+this->node_.get()->getName()+std::string(")");
    }
};

}  // namespace cats
