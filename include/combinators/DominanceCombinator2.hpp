#pragma once

#include <set>
#include <vector>
#include <string>
#include <math.h>
#include <iomanip>

#include "../SearchManager.hpp"
#include "../Store.hpp"
#include "../Combinator.hpp"
#include "../../include/numeric.hpp"
#include "../io.hpp"
#include "../PrefixEquivalence.hpp"


namespace cats {


/** \class DominanceCombinator2
 * \brief eliminates nodes that are dominated by some other node already seen and stored.
 */
template <typename PE, typename SP = int>
class DominanceCombinator2 : public Combinator<PE, SP> {
 protected:
    PrefixEquivalenceStore<PE, DominanceInfos2>& storage_;
    IterNumber iter_number_;

 public:
    DominanceCombinator2(NodePtr node, SearchManager& manager, PrefixEquivalenceStore<PE, DominanceInfos2>& store,
    IterNumber it=0) : Combinator<PE, SP>(node, manager), storage_(store), iter_number_(it) {}

    DominanceCombinator2(const DominanceCombinator2<PE>& n)
    : Combinator<PE, SP>(n), storage_(n.storage_), iter_number_(n.iter_number_) {}

    NodePtr copy() const override { return NodePtr(new DominanceCombinator2(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> filteredChildren;
        PE pe = this->getPrefixEquivalence();
        if ( storage_.contains(pe) ) {  // if storage contains node check if it should be cut
            DominanceInfos2& pe_infos = storage_.get(pe);
            double pe_eval = this->PEevalPrefix();
            bool stricly_dominated = this->manager_.isStrictlyBetter(pe_infos.prefix_primal, pe_eval);
            bool iter_equal = pe_infos.prefix_primal == pe_eval && pe_infos.iter_number == iter_number_;
            if ( stricly_dominated || iter_equal ) {  // if current node is dominated, cut node
                storage_.registerCut();
                return filteredChildren;  // stop there, no need to expand children
            }
            if ( this->manager_.isStrictlyBetter(pe_eval, pe_infos.prefix_primal) ) {  // if node has a better prefix, store it
                pe_infos.prefix_primal = pe_eval;
                pe_infos.iter_number = iter_number_;
                // storage_.set(pe, pe_infos);
            }
        } else {  // if storate does not contains node, add it
            storage_.set(pe, {.prefix_primal = this->PEevalPrefix(), .iter_number = iter_number_});
        }
        // enumerate children while cutting if prefix equivalence dominated
        for (auto& child : this->node_.get()->getChildren()) {
            filteredChildren.push_back(createPrefixEquivalence(child));
        }
        return filteredChildren;
    }

    std::string getName() override {
        return std::string("D2(")+this->node_.get()->getName()+std::string(")");
    }

    inline void newIter() override {
        this->node_.get()->newIter();
        iter_number_++;
    }

 protected:
    inline NodePtr createPrefixEquivalence(NodePtr n) {
        return NodePtr(new DominanceCombinator2(n, this->manager_, storage_, iter_number_));
    }
};

}  // namespace cats
