#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include "../SearchManager.hpp"
#include "../Combinator.hpp"

namespace cats {

/**
 * \class BeamCombinator
 * \brief implements a "beam" node. Original Idea from https://www.aaai.org/Papers/ICAPS/2005/ICAPS05-010.pdf
 */
template <typename PE = int, typename SolPart = int>
class BeamCombinator : public Combinator<PE, SolPart> {
 public:
    std::vector<NodePtr> nodes_;
    std::vector<NodePtr> expanded_;
    int d_;
    double smallest_heuristic_cut_;
    std::vector<NodePtr> admissible_nodes_;
    bool is_expanded_;
    bool guide_computed_;
    bool guide_;

 public:
    BeamCombinator(NodePtr node, SearchManager& manager, int d) : Combinator<PE, SolPart>(node, manager), d_(d), is_expanded_(false), guide_computed_(false) {
        nodes_.push_back(node);
    }

    BeamCombinator(const BeamCombinator& n, int d) : Combinator<PE, SolPart>(n, n.manager_), d_(d), is_expanded_(false), guide_computed_(false) {
        nodes_.push_back(n);
    }

    BeamCombinator(const BeamCombinator& n, std::vector<NodePtr> nodes, int d)
        : Combinator<PE, SolPart>(n), nodes_(nodes), d_(d), is_expanded_(false), guide_computed_(false) {}

    BeamCombinator(const BeamCombinator& n)
        : Combinator<PE, SolPart>(n), nodes_(n.nodes_), d_(n.d_), is_expanded_(n.is_expanded_), guide_computed_(n.guide_computed_) {}

    NodePtr copy() const override { return NodePtr(new BeamCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        assert(!nodes_.empty());
        std::vector<NodePtr> res;
        if ( !isGoal() ) {
            assert(!isGoal());
            assert(nodes_.size() > 1 || nodes_[0].get()->isGoal() != true);
            if ( !is_expanded_ ) expand();  // if expand not already done, do it
            // add admissible nodes to res
            if ( !admissible_nodes_.empty() ) {  // if there are some admissible solutions, put the best in the children
                NodePtr best_found = admissible_nodes_[0];
                for ( NodePtr admissible_node : admissible_nodes_ ) {
                    if ( admissible_node.get()->evaluate() < best_found.get()->evaluate() ) {
                        best_found = admissible_node;
                    }
                }
                std::vector<NodePtr> tmp_vec;
                tmp_vec.push_back(best_found);
                res.push_back(NodePtr(new BeamCombinator(*this, tmp_vec, d_)));
            }
            // std::cout << "getChildren: " << expanded_.size() << " elements\n";
            // expand nodes and add "jumbo" nodes to res
            std::vector<NodePtr> tmp;
            for ( auto e : expanded_ ) {
                tmp.push_back(e);
                if ( static_cast<int>(tmp.size()) == d_ ) {  // creates a BeamCombinator child and reset tmp
                    res.push_back(NodePtr(new BeamCombinator(*this, tmp, d_)));
                    tmp = std::vector<NodePtr>();
                }
            }
        }
        return res;
    }

    double evalPrefix() const override {
        assert(!nodes_.empty());
        double res = nodes_[0].get()->evalPrefix();
        for ( auto e : nodes_ ) {
            res = std::min(res, e.get()->evalPrefix());
        }
        return res;
    }

    double evalSuffix() const override {
        assert(!nodes_.empty());
        double res = nodes_[0].get()->evalSuffix();
        for ( auto e : nodes_ ) {
            res = std::min(res, e.get()->evalSuffix());
        }
        return res;
    }

    /** \brief returns the minimum evaluation on nodes in memory
     */
    double evaluate() const override {
        assert(!nodes_.empty());
        double res = nodes_[0].get()->evaluate();
        for ( auto e : nodes_ ) {
            res = std::min(res, e.get()->evaluate());
        }
        return res;
    }

    /** \brief returns the minimum guide of nodes in memory
     * TODO(all) implement variants. *i.e.* min(guides) or avg(guides) 
     */
    double guide() override {
        return this->node_.get()->guide();
    }

    /** \brief true if only one node and it is admissible
     */
    bool isGoal() const override {
        assert(!nodes_.empty());
        return nodes_.size() == 1 && nodes_[0].get()->isGoal();
    }

    std::string getName() override {
        assert(!nodes_.empty());
        return std::string("Beam(")+this->node_.get()->getName()+std::string(",")+std::to_string(d_)+std::string(")");
    }

 private:
    /**
     * \brief executes a Beam Search starting from nodes_ until it reaches D nodes.
     */
    void expand() {
        // create expanded_
        expanded_ = nodes_;
        while ( static_cast<int>(expanded_.size()) <= d_ ) {  // expands until there are D nodes
            // expand nodes in a temporary vector
            std::vector<NodePtr> tmp;
            for ( auto e : expanded_ ) {
                for ( auto c : e.get()->getChildren() ) {
                    if ( c.get()->isGoal() ) {
                        admissible_nodes_.push_back(c);
                    } else {
                        tmp.push_back(c);
                    }
                }
            }
            if ( tmp.empty() ) {  // if no more nodes to expand
                break;
            }
            // copy in expanded_
            expanded_.swap(tmp);
        }
        // sort expanded_
        std::sort(std::begin(expanded_), std::end(expanded_), [](NodePtr a, NodePtr b) {
            return a.get()->guide() < b.get()->guide();
        });
        is_expanded_ = true;
    }
};

}  // namespace cats
