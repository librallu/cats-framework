#pragma once

#include <set>
#include <vector>
#include <string>
#include <math.h>
#include <iomanip>

#include "../SearchManager.hpp"
#include "../Store.hpp"
#include "../Combinator.hpp"
#include "../../include/numeric.hpp"
#include "../io.hpp"
#include "../PrefixEquivalence.hpp"


namespace cats {


/** \class DominanceCombinator
 * \brief eliminates nodes that are dominated by some other node already seen and stored.
 */
template <typename PE, typename SP = int>
class DominanceCombinator : public Combinator<PE, SP> {
 protected:
    PrefixEquivalenceStore<PE, DominanceInfos>& storage_;

 public:
    DominanceCombinator(NodePtr node, SearchManager& manager, PrefixEquivalenceStore<PE, DominanceInfos>& store)
        : Combinator<PE, SP>(node, manager), storage_(store) {}

    DominanceCombinator(const DominanceCombinator<PE>& n)
        : Combinator<PE, SP>(n), storage_(n.storage_) {}

    NodePtr copy() const override { return NodePtr(new DominanceCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> filteredChildren;
        PE pe = this->getPrefixEquivalence();
        if ( storage_.contains(pe) ) {  // if storage contains node check if it should be cut
            DominanceInfos& pe_infos = storage_.get(pe);
            if ( this->manager_.isStrictlyBetter(pe_infos.prefix_primal, this->PEevalPrefix()) ) {  // if current node is dominated, cut node
                storage_.registerCut();
                return filteredChildren;  // stop there, no need to expand children
            }
            if ( this->manager_.isStrictlyBetter(this->PEevalPrefix(), pe_infos.prefix_primal) ) {  // if node has a better prefix, store it
                pe_infos.prefix_primal = this->PEevalPrefix();
                // storage_.set(pe, pe_infos);
            }
        } else {  // if storate does not contains node, add it
            storage_.set(pe, {.prefix_primal = this->PEevalPrefix()});
        }
        // enumerate children while cutting if prefix equivalence dominated
        for (auto& child : this->node_.get()->getChildren()) {
            filteredChildren.push_back(createPrefixEquivalence(child));
        }
        return filteredChildren;
    }

    std::string getName() override {
        return std::string("D(")+this->node_.get()->getName()+std::string(")");
    }

 protected:
    inline NodePtr createPrefixEquivalence(NodePtr n) {
        return NodePtr(new DominanceCombinator(n, this->manager_, storage_));
    }
};

}  // namespace cats
