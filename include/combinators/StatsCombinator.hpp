#pragma once

#include "../SearchManager.hpp"
#include "../Node.hpp"
#include "../SearchStats.hpp"
#include "../Combinator.hpp"

namespace cats {

/**
 * \brief Implements a Statistic combinator. It aims to provide statistics about the search.
 */
template <typename PE = int, typename SP = int>
class StatsCombinator : public Combinator<PE, SP> {
 protected:
    SearchManager& manager_;
    SearchStats& stats_;
    bool register_node_events_;
    NodeNumber id_;
    Depth depth_;

 public:
    explicit StatsCombinator(NodePtr node, SearchManager& manager,
    SearchStats& stats, bool register_node_events = false, bool root = true, NodeNumber parentid = 0) :
    Combinator<PE, SP>(node, manager), manager_(manager), stats_(stats), register_node_events_(register_node_events), depth_(0) {
        stats_ = manager_.getSearchStats();
        if ( register_node_events_ ) {
            id_ = stats_.get_nb_generated();
            stats_.addNodeEvent({
                .nodeid = id_,
                .parent = root ? id_ : parentid,
                .g = this->node_.get()->evalPrefix(),
                .h = this->node_.get()->evalSuffix(),
                .guide = this->node_.get()->guide(),
                .nodemsg = this->node_.get()->nodemsg(),
                .arcmsg = this->node_.get()->arcmsg(),
                .goal = this->node_.get()->isGoal()
            });
        }
        stats_.add_nb_generated();
    }

    StatsCombinator(const StatsCombinator& n) : Combinator<PE, SP>(n),
    manager_(n.manager_), stats_(n.stats_), register_node_events_(n.register_node_events_), depth_(n.depth_) {
        if ( register_node_events_ ) {
            id_ = stats_.get_nb_generated();
            stats_.addNodeEvent({
                .nodeid = id_,
                .parent = n.id_,
                .g = this->node_.get()->evalPrefix(),
                .h = this->node_.get()->evalSuffix(),
                .guide = this->node_.get()->guide(),
                .nodemsg = this->node_.get()->nodemsg(),
                .arcmsg = this->node_.get()->arcmsg(),
                .goal = this->node_.get()->isGoal()
            });
        }
        stats_.add_nb_generated();
    }

    void set_depth(Depth d) { depth_ = d; }

    NodePtr copy() const override { return NodePtr(new StatsCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for ( NodePtr a : this->node_.get()->getChildren() ) {
            StatsCombinator* child_ptr = new StatsCombinator(a, this->manager_, this->stats_, register_node_events_, false, id_);
            child_ptr->set_depth(depth_+1);
            res.push_back(NodePtr(child_ptr));
        }
        if ( res.empty() ) stats_.add_nb_trashed();
        stats_.add_nb_expanded();
        return res;
    }


    double evalPrefix() const override {
        stats_.add_eval();
        return this->node_.get()->evalPrefix();
    }

    double evalSuffix() const override {
        stats_.add_eval();
        return this->node_.get()->evalSuffix();
    }

    double evaluate() const override {
        stats_.add_eval();
        return this->node_.get()->evaluate();
    }

    double guide() override {
        stats_.add_guide();
        return this->node_.get()->guide();
    }

    bool isGoal() const override {
        if ( this->node_.get()->isGoal() ) {
            // if better than the last one, register it in the SearchStats
            if ( stats_.noObjRegistered() || manager_.isStrictlyBetter(this->evaluate(), stats_.lastObjRegistered().v) ) {
                stats_.addObj(manager_.getElapsedTime(), this->evaluate());
            }
            return true;
        } else {
            return false;
        }
    }

    std::string getName() override {
        return "("+this->node_.get()->getName()+")";
    }
};

}  // namespace cats
