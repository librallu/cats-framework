#pragma once


#include <set>
#include <vector>
#include <string>
#include <cstdlib>
#include <algorithm>

#include "../SearchManager.hpp"
#include "../Combinator.hpp"
#include "../numeric.hpp"

namespace cats {

/**
 * \brief reduces children of a node to the D best children.
 */
template <typename PE = int, typename SolPart = int>
class IterativeBroadeningCombinator : public Combinator<PE, SolPart> {
 public:
    int D_;

 public:
    IterativeBroadeningCombinator(NodePtr node, SearchManager& manager, int D) : Combinator<PE, SolPart>(node, manager), D_(D) {}

    IterativeBroadeningCombinator(const IterativeBroadeningCombinator& n) : Combinator<PE, SolPart>(n), D_(n.D_) {}

    NodePtr copy() const override { return NodePtr(new IterativeBroadeningCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> children = this->node_.get()->getChildren();
        std::vector<NodePtr> res;
        std::sort(std::begin(children), std::end(children), [](NodePtr a, NodePtr b) {
            return a.get()->guide() < b.get()->guide();
        });
        int l = std::min<int>(D_, children.size());
        for ( int i = 0 ; i < l ; i++ ) {
            res.push_back(NodePtr( new IterativeBroadeningCombinator(children[i], this->manager_, D_) ));
        }
        return res;
    }

    std::string getName() override {
        return std::string("IB(")+this->node_.get()->getName()+","+std::to_string(D_)+std::string(")");
    }
};

}  // namespace cats
