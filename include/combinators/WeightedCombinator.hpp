#pragma once


#include <set>
#include <vector>
#include <string>
#include <cstdlib>
#include <algorithm>

#include "../SearchManager.hpp"
#include "../Combinator.hpp"
#include "../io.hpp"

namespace cats {

/**
 * \brief define guide as f(n) = g(n)+w.h(n)
 */
template <typename PE = int, typename SolPart = int>
class WeightedCombinator : public Combinator<PE, SolPart> {
 public:
    float w_;

 public:
    WeightedCombinator(NodePtr node, float w) : Combinator<PE, SolPart>(node), w_(w) {}

    WeightedCombinator(const WeightedCombinator& n) : Combinator<PE, SolPart>(n), w_(n.w_) {}

    NodePtr copy() const override { return NodePtr(new WeightedCombinator(*this)); }

    std::vector<NodePtr> getChildren(double& smallest_heuristic_cut) override {
        std::vector<NodePtr> res;
        for ( auto e : this->node_.get()->getChildren(smallest_heuristic_cut) ) {
            res.push_back(NodePtr( new WeightedCombinator(e, w_) ));
        }
        return res;
    }

    double guide() override {
        return this->evalPrefix() + w_*this->evalSuffix();
    }

    std::string getName() override {
        return "w("+this->node_.get()->getName()+","+getDoubleAtPrecision(w_, 2)+")";
    }
};

}  // namespace cats
