#pragma once

#include <set>
#include <vector>
#include <string>
#include <math.h>
#include <iomanip>

#include "../SearchManager.hpp"
#include "../Store.hpp"
#include "../Combinator.hpp"
#include "../../include/numeric.hpp"
#include "../io.hpp"
#include "../PrefixEquivalence.hpp"


namespace cats {

typedef std::vector<int32_t> Front;

struct FrontVector {
    std::vector<Front> elts;
    IterNumber iter;
};

/** \class BucketListCombinator
 * \brief stores for each prefix equivalence a set of pareto fronts. Prune the current node if dominated by one of the stored elements.
 */
template <typename PE, typename SP = int>
class BucketListCombinator : public Combinator<PE, SP> {
 protected:
    PrefixEquivalenceStore<PE, FrontVector>& store_;
    IterNumber iter_number_;

 public:
    BucketListCombinator(NodePtr node, SearchManager& manager, PrefixEquivalenceStore<PE, FrontVector>& store,
    IterNumber it=0) : Combinator<PE, SP>(node, manager), store_(store), iter_number_(it) {}

    BucketListCombinator(const BucketListCombinator<PE>& n)
    : Combinator<PE, SP>(n), store_(n.store_), iter_number_(n.iter_number_) {}

    NodePtr copy() const override { return NodePtr(new BucketListCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        // 1. first: check if the current node is dominated and manage memory
        std::vector<NodePtr> filteredChildren;
        PE pe = this->getPrefixEquivalence();
        if ( store_.contains(pe) ) {  // if storage contains node check if it should be pruned
            FrontVector& fronts = store_.get(pe);
            Front front = this->getParetoFronts();
            bool iter_equal = fronts.iter == iter_number_;
            for ( Front& f : fronts.elts ) {
                assert(f.size() == front.size());
                bool dom_strictly = false;
                bool dominates = true;
                for ( int i = 0 ; i < static_cast<int>(front.size()) ; i++ ) {
                    if ( f[i] > front[i] ) {  // f does not dominate front
                        dominates = false;
                        break;
                    }
                    if ( f[i] < front[i] ) dom_strictly = true;  // f could strictly dominate front
                }
                if ( dominates && (dom_strictly||iter_equal) ) {
                    store_.registerCut();
                    return filteredChildren;
                }
            }
            // then update the store if needed
            // NAIVE STRATEGY: use only one front (minimum sum of fronts)
            // 1. count sums
            int_fast32_t sum_current = 0;
            int_fast32_t sum_stored = 0;
            for ( int i = 0 ; i < static_cast<int>(front.size()) ; i++ ) {
                sum_current += front[i];
                sum_stored += fronts.elts[0][i];
            }
            if ( sum_current < sum_stored ) {
                std::vector<Front> res;
                res.push_back(this->getParetoFronts());
                store_.set(pe, {elts: res, iter: iter_number_});
            }
            // TODO(all) test other mechanisms

        } else {  // if storate does not contains node, add it
            std::vector<Front> res;
            res.push_back(this->getParetoFronts());
            store_.set(pe, {elts: res, iter: iter_number_});
        }
        // enumerate children while cutting if prefix equivalence dominated
        for (auto& child : this->node_.get()->getChildren()) {
            filteredChildren.push_back(createBucketListCombinator(child));
        }
        return filteredChildren;
    }

    std::string getName() override {
        return std::string("buck(")+this->node_.get()->getName()+std::string(")");
    }

    inline void newIter() override {
        this->node_.get()->newIter();
        iter_number_++;
    }

 protected:
    inline NodePtr createBucketListCombinator(NodePtr n) {
        return NodePtr(new BucketListCombinator(n, this->manager_, store_, iter_number_));
    }
};

}  // namespace cats
