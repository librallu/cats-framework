#pragma once

#include <set>
#include <vector>
#include <string>
#include <math.h>
#include <iomanip>
#include <unordered_map>

#include "../SearchManager.hpp"
#include "../Store.hpp"
#include "../Combinator.hpp"
#include "../../include/numeric.hpp"
#include "../io.hpp"
#include "../PrefixEquivalence.hpp"


namespace cats {


enum StoragePolicy {
  ALL  // store information at each explored node
};


/** \class PrefixEquivalenceCombinator
 * \brief eliminates nodes that are dominated by some other node already seen and stored.
 * Stores nodes according to a storage policy given as an input.
 */
template <typename PE, typename SP = int>
class PrefixEquivalenceCombinator : public Combinator<PE, SP> {
 protected:
    PrefixEquivalenceStore<PE>& storage_;
    StoragePolicy type_;
    bool cuts_enabled_;
    bool use_stored_suffix_dual_;   ///< if true, exploits suffix bound in memory if available. Allows to mix branching schemes with strong bound computation
    bool report_primal_bounds_;  ///< if true, creates a upper bound information in the prefix equivalence when a new soution is found

 public:
    PrefixEquivalenceCombinator(NodePtr node, SearchManager& manager, PrefixEquivalenceStore<PE>& store, StoragePolicy type = StoragePolicy::ALL)
        : Combinator<PE, SP>(node, manager), storage_(store), type_(type), cuts_enabled_(true),
        use_stored_suffix_dual_(true), report_primal_bounds_(false) {}

    PrefixEquivalenceCombinator(const PrefixEquivalenceCombinator<PE>& n)
        : Combinator<PE, SP>(n), storage_(n.storage_), type_(n.type_), cuts_enabled_(n.cuts_enabled_),
        use_stored_suffix_dual_(n.use_stored_suffix_dual_), report_primal_bounds_(n.report_primal_bounds_) {}

    NodePtr copy() const override { return NodePtr(new PrefixEquivalenceCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> filteredChildren;
        PE pe = this->getPrefixEquivalence();
        if ( storage_.contains(pe) ) {  // if storage contains node check if it should be cut
            PrefixEquivalenceInfos pe_infos = storage_.get(pe);
            if ( cuts_enabled_ && this->manager_.isStrictlyBetter(pe_infos.prefix_primal, this->PEevalPrefix()) ) {  // if current node is dominated, cut node
                storage_.registerCut();
                return filteredChildren;  // stop there, no need to expand children
            }
            if ( this->manager_.isStrictlyBetter(this->PEevalPrefix(), pe_infos.prefix_primal) ) {  // if node has a better prefix, store it
                pe_infos.prefix_primal = this->PEevalPrefix();
                storage_.set(pe, pe_infos);
            }
        } else {  // if storate does not contains node, add it
            storage_.set(pe, {.prefix_primal = this->PEevalPrefix(), .prefix_dual = 0., .suffix_dual = this->evalSuffix(), MAX_DOUBLE});
        }
        // enumerate children while cutting if prefix equivalence dominated
        for (auto& child : this->node_.get()->getChildren()) {
            filteredChildren.push_back(createPrefixEquivalence(child));
        }
        return filteredChildren;
    }

    void set_enable_cuts(bool v) override {
        cuts_enabled_ = v;
        this->node_.get()->set_enable_cuts(v);
    }

    std::string getName() override {
        PE pe = this->getPrefixEquivalence();
        return std::string("PE(")+this->node_.get()->getName()+std::string(")");
    }

    double evaluate() const override {
        if ( use_stored_suffix_dual_ ) {
            PE pe = this->getPrefixEquivalence();
            if ( storage_.contains(pe) ) {
                PrefixEquivalenceInfos pe_infos = storage_.get(pe);
                return this->evalPrefix() + pe_infos.suffix_dual;
            }
            return this->node_.get()->evaluate();
        } else {
            return this->node_.get()->evaluate();
        }
    }

    double guide() override {
        return this->node_.get()->guide();
    }

 protected:
    NodePtr createPrefixEquivalence(NodePtr n) {
        NodePtr res = NodePtr(new PrefixEquivalenceCombinator(n, this->manager_, storage_, type_));
        res.get()->set_enable_cuts(cuts_enabled_);
        if ( report_primal_bounds_ && res.get()->isGoal() ) {  // report primal bounds when a solution is found
            double eval = res.get()->evaluate();
            for ( auto e : this->getAllParents() ) {
                PE pe = dynamic_cast<PrefixEquivalenceNode<PE>*>(e.get())->getPrefixEquivalence();
                double eval_e = dynamic_cast<PrefixEquivalenceNode<PE>*>(e.get())->PEevalPrefix();
                if ( storage_.contains(pe) ) {
                    PrefixEquivalenceInfos pe_infos = storage_.get(pe);
                    pe_infos.suffix_primal = std::min(pe_infos.suffix_primal, eval-eval_e);
                    storage_.set(pe, pe_infos);
                } else {
                    storage_.set(pe, {eval_e, 0, e.get()->evalSuffix(), eval-eval_e});
                }
            }
        }
        return res;
    }
};

}  // namespace cats
