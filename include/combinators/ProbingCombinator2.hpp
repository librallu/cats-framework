#pragma once

#include "../SearchManager.hpp"
#include "../Combinator.hpp"
#include "../Node.hpp"
#include "../TreeSearch.hpp"
#include "../treesearch/Greedy.hpp"
#include "../numeric.hpp"
#include "../combinators/PrefixEquivalenceCombinator.hpp"

namespace cats {

/** \class ProbingCombinator2
 * \brief combinator that modifies the guide according to the evaluation of a greedy algorithm
 * if alpha = 0 : use only lower bound, if alpha = 1, use only upper bound
 */
template <typename T = int, typename SP = int>
class ProbingCombinator2 : public Combinator<T, SP> {
 private:
    double alpha_;
    TreeSearchParameters params_;
    double time_limit_;
    double ub_ = MAX_DOUBLE;
    std::string* algo_msg_;
    PrefixEquivalenceStore<T>* pe_store_;


 public:
    ProbingCombinator2(NodePtr node, SearchManager& manager, double alpha, TreeSearchParameters params, double time_limit, PrefixEquivalenceStore<T>* pe_store = nullptr, std::string* algo_msg = nullptr) :
        Combinator<T, SP>(node, manager), alpha_(alpha), params_(params), time_limit_(time_limit),
        algo_msg_(algo_msg), pe_store_(pe_store) {}

    ProbingCombinator2(const ProbingCombinator2& n) :
        Combinator<T, SP>(n), alpha_(n.alpha_),
        params_(n.params_), time_limit_(n.time_limit_), algo_msg_(n.algo_msg_), pe_store_(n.pe_store_)  {}

    /******* IMPLEMENTATION OF BRANCHING SCHEME INTERFACE *******/
    NodePtr copy() const override { return NodePtr(new ProbingCombinator2(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for ( NodePtr a : this->node_.get()->getChildren() ) {
            res.push_back(NodePtr(new ProbingCombinator2(a, this->manager_, alpha_, params_, time_limit_, pe_store_, algo_msg_)));
        }
        return res;
    }

    void setAlpha(double alpha) {
        alpha_ = alpha;
    }

    double guide() override {
        // std::cout << "START PROBING\n";
        double lb = this->node_.get()->evaluate();
        bool perform_probing = true;
        if ( alpha_ < 0.0001 ) {
            perform_probing = false;
        }
        if ( ub_ >= MAX_DOUBLE && perform_probing ) {
            // if prefix equivalence store, use it to retreive stored info
            if ( pe_store_ != nullptr ) {
                T pe = this->getPrefixEquivalence();
                if ( pe_store_->contains(pe) ) {
                    PrefixEquivalenceInfos pe_infos = pe_store_->get(pe);
                    if ( pe_infos.suffix_primal < MAX_DOUBLE ) {
                        ub_ = this->evalPrefix() + pe_infos.suffix_primal;
                        perform_probing = false;
                    }
                }
            }
            if ( perform_probing ) {
                TreeSearchParameters greedy_params = {
                    .root = this->node_.get()->copy(),
                    .manager = params_.manager,
                    .id = params_.id
                };
                greedy_params.root.get()->set_enable_cuts(false);
                Greedy greedy = Greedy(greedy_params);
                greedy.set_check_dominance(false);  // disable dominance check
                greedy.set_measure_heuristic_cuts(false);  // disable global lb computation
                if ( algo_msg_ == nullptr ) {
                    greedy.set_msg(" prob");
                } else {
                    std::string msg = *algo_msg_ + std::string(" prob");
                    greedy.set_msg(msg);
                }
                NodePtr res_greedy = greedy.run(time_limit_);  // run greedy algorithm
                if ( res_greedy != nullptr && res_greedy.get() != nullptr ) {
                    ub_ = res_greedy.get()->evaluate();
                    if ( pe_store_ != nullptr ) {  // update prefix equivalence store for each parent of the successful probing
                        for ( NodePtr parent : res_greedy.get()->getAllParents() ) {  // for each parent
                            double eval_prefix = parent->evalPrefix();
                            T pe = dynamic_cast<PrefixEquivalenceNode<T>*>(parent.get())->getPrefixEquivalence();
                            if ( pe_store_->contains(pe) ) {
                                PrefixEquivalenceInfos pe_infos = pe_store_->get(pe);
                                pe_infos.suffix_primal = std::min(pe_infos.suffix_primal, ub_-eval_prefix);
                                pe_store_->set(pe, pe_infos);
                            } else {  // create a PE
                                pe_store_->set(pe, {.prefix_primal = eval_prefix, .prefix_dual = 0., .suffix_dual = parent->evalSuffix(), .suffix_primal = ub_-eval_prefix});
                            }
                        }
                    }
                }
            }
        }
        if ( alpha_ < 0.0001 ) {
            ub_ = 0;
        }
        return (1.-alpha_)*lb + alpha_*ub_;
    }

    std::string getName() override {
        return std::string("P2(")+this->node_.get()->getName()+std::string(")");
    }

    void setTreeSearchName(std::string* alg_name) override {
        algo_msg_ = alg_name;
        this->node_.get()->setTreeSearchName(alg_name);
    }
};

}  // namespace cats
