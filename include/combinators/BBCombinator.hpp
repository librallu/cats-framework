#pragma once

#include "../SearchManager.hpp"
#include "../Node.hpp"
#include "../SearchStats.hpp"
#include "../Combinator.hpp"

namespace cats {

/** \class BBCombinator
 * \brief eliminates nodes that are dominated by the best known solution.
 */
template <typename PE = int, typename SP = int>
class BBCombinator : public Combinator<PE, SP> {
 protected:
    SearchManager& manager_;

 public:
    explicit BBCombinator(NodePtr node, SearchManager& manager) :
    Combinator<PE, SP>(node, manager), manager_(manager) {}

    BBCombinator(const BBCombinator& n) : Combinator<PE, SP>(n), manager_(n.manager_) {}

    NodePtr copy() const override { return NodePtr(new BBCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for ( NodePtr a : this->node_.get()->getChildren() ) {
            if (!manager_.isDominatedByBest(a)) {
                BBCombinator* child_ptr = new BBCombinator(a, this->manager_);
                res.push_back(NodePtr(child_ptr));
            } else {
                stats().add_nb_pruned();
            }
        }
        return res;
    }

    inline std::string getName() override {
        return "B("+this->node_.get()->getName()+")";
    }

 private:
    inline SearchStats& stats() { return manager_.getSearchStats(); }
};

}  // namespace cats
