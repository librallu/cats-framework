#pragma once


#include <set>
#include <vector>
#include <string>
#include <math.h>

#include "../SearchManager.hpp"
#include "../Combinator.hpp"
#include "../TreeSearch.hpp"

namespace cats {

enum DiscrepancyEvolution { CST, LIN, QUAD, CUB, RELATIVE_LIN, RELATIVE_QUAD, SIGMOID };


/** \class LDSCombinator
  * \brief eliminates the children that are far from the heuristic. Creates a smaller branching scheme that can be explored with DFS.
  */
template <typename PE = int, typename SolPart = int>
class LDSCombinator : public Combinator<PE, SolPart> {
 private:
    int d_;
    int initial_d_;
    DiscrepancyEvolution type_;

 public:
    LDSCombinator(NodePtr node, SearchManager& manager, int d, DiscrepancyEvolution type = DiscrepancyEvolution::LIN)
        : Combinator<PE, SolPart>(node, manager), d_(d), initial_d_(d), type_(type) {}

    LDSCombinator(NodePtr node, SearchManager& manager, int d, int initial_d, DiscrepancyEvolution type = DiscrepancyEvolution::LIN)
        : Combinator<PE, SolPart>(node, manager), d_(d), initial_d_(initial_d), type_(type) {}

    LDSCombinator(const LDSCombinator& n) : Combinator<PE, SolPart>(n), d_(n.d_), initial_d_(n.initial_d_), type_(n.type_) {}

    NodePtr copy() const override { return NodePtr(new LDSCombinator(*this)); }

    std::vector<NodePtr> getChildren() override {
        // create sorted list of children
        std::multiset<NodePtr, Compare> children(Compare(this->manager_));
        for (auto child : this->node_.get()->getChildren()) {
            children.insert(child);
            if (static_cast<int>(children.size()) > d_+1) {
                NodePtr child = *std::prev(children.end());
                children.erase(std::prev(children.end()));
            }
        }
        // compute upper and lower values on children
        double lowest = 0., highest = 0.;
        if (!children.empty()) {
            NodePtr s = *children.begin();
            lowest = s.get()->evaluate();
            s = *children.rbegin();
            highest = s.get()->evaluate();
        }
        // keep only children with not too much discrepancies
        int i = 0;
        std::vector<NodePtr> res;
        while ( !children.empty() ) {
            NodePtr s = *children.begin();
            children.erase(children.begin());
            int new_discrepancies = remainingDiscrepancy(i++, s.get()->evaluate(), lowest, highest);
            if (new_discrepancies < 0) {
                break;
            }
            NodePtr child(new LDSCombinator(s, this->manager_, new_discrepancies, initial_d_, type_));
            res.push_back(child);
        }
        return res;
    }

    /** \fn remainingDiscrepancy
     * \brief returns the number of discrepancies remaining after this node
     */
    int remainingDiscrepancy(int i, double eval, double l, double h) {
        int res = 0;
        switch (type_) {
            case DiscrepancyEvolution::CST:
                res = (i > 0)?d_-1:d_;
            break;
            case DiscrepancyEvolution::LIN:
                res = d_ - i;
            break;
            case DiscrepancyEvolution::QUAD:
                res = d_-(i*i);
            break;
            case DiscrepancyEvolution::CUB:
                res = d_-(i*i*i);
            break;
            case DiscrepancyEvolution::RELATIVE_LIN:  // TODO(malik) Compare to eject the "worst" if value equality
                if (h != l)
                    res = d_*(1-((eval-l)/(h-l)));
                else
                    res = d_ - i;
            break;
            case DiscrepancyEvolution::RELATIVE_QUAD:
                if (h == l) {
                    res = d_;
                } else {
                    double c = ((eval-l)/(h-l));
                    res = d_*(1-c*c);
                }
            break;
            case DiscrepancyEvolution::SIGMOID:
                if (h == l) {
                    res = d_;
                } else {
                    int lambda = -2;
                    double c = ((eval-l)/(h-l));
                    c = 2*c-1;
                    res = d_*(1/(1+std::exp(lambda*c)));
                }
            break;
        }
        return res;
    }


    std::string getName() override {
        return std::string("LDS(")+this->node_.get()->getName()+std::string(",")+std::to_string(initial_d_)+std::string(")");
    }

    std::string DiscrepancyEvolutionToString(DiscrepancyEvolution x) {
        std::string res = "unknown";
        switch (x) {
            case DiscrepancyEvolution::CST:
                res = "Cst";
            break;
            case DiscrepancyEvolution::LIN:
                res = "Linear";
            break;
            case DiscrepancyEvolution::QUAD:
                res = "Quadratic";
            break;
            case DiscrepancyEvolution::CUB:
                res = "Cubic";
            break;
            case DiscrepancyEvolution::RELATIVE_LIN:
                res = "Relative_Linear";
            break;
            case DiscrepancyEvolution::RELATIVE_QUAD:
                res = "Relative_Quadratic";
            break;
            case DiscrepancyEvolution::SIGMOID:
                res = "Sigmoid";
            break;
        }
        return res;
    }
};

}  // namespace cats
