#pragma once

#include "SearchManager.hpp"
#include "Node.hpp"

namespace cats {


/**
 * \brief abstract class that implements combinators
 * A combinator is interleaved between the user defined Node and the
 * generic tree searches. It adds additional behaviour like LDS discrepancy counts, pheromones guides, probing etc.
 */
template <typename PE = int, typename SolPart = int>
class Combinator : public PrefixEquivalenceNode<PE>, public SolutionPartsNode<SolPart> {
 protected:
    NodePtr node_;
    SearchManager& manager_;

 public:
    explicit Combinator(NodePtr node, SearchManager& manager) : Node(), node_(node), manager_(manager) {}

    Combinator(const Combinator& n) : Node(n), node_(n.node_.get()->copy()), manager_(n.manager_) {}

    NodePtr copy() const override { return NodePtr(new Combinator(*this)); }


    std::string solToStr() override {return node_.get()->solToStr();}

    void writeSol(std::string filename) override {
        return node_.get()->writeSol(filename);
    }


    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for ( NodePtr a : node_.get()->getChildren() ) {
            res.push_back(NodePtr(new Combinator(a, this->manager_)));
        }
        return res;
    }

    double evalPrefix() const override {
        return node_.get()->evalPrefix();
    }

    double evalSuffix() const override {
        return node_.get()->evalSuffix();
    }

    double evaluate() const override {
        return node_.get()->evaluate();
    }

    double guide() override {
        return node_.get()->guide();
    }

    bool isGoal() const override {
        return node_.get()->isGoal();
    }

    void handleNewBest() override {
        node_.get()->handleNewBest();
    }

    void handleEndSearch() override {
        node_.get()->handleEndSearch();
    }

    std::string getName() override {
        return "("+node_.get()->getName()+")";
    }

    void set_enable_cuts(bool v) override {
        node_.get()->set_enable_cuts(v);
    }

    void setTreeSearchName(std::string* alg_name) override {
        node_.get()->setTreeSearchName(alg_name);
    }

    NodePtr getOriginalBS() override {
        return node_.get()->getOriginalBS();
    }

    std::vector<int32_t> getParetoFronts() override {
        return node_.get()->getParetoFronts();
    }

    std::string nodemsg() override {
        return node_.get()->nodemsg();
    }

    virtual std::string arcmsg() override {
        return node_.get()->arcmsg();
    }

    virtual void newIter() override {
        node_.get()->newIter();
    }

    bool hasParent() const override {
        return node_.get()->hasParent();
    }

    NodePtr getParent() const override {
        NodePtr n = node_.get()->getParent();
        return NodePtr(new Combinator(n, manager_));
    }

    std::vector<NodePtr> getAllParents() const override {
        return node_.get()->getAllParents();
    }

    PE getPrefixEquivalence() const override {
        return dynamic_cast<PrefixEquivalenceNode<PE>*>(node_.get())->getPrefixEquivalence();
    }

    double PEevalPrefix() const override {
        return dynamic_cast<PrefixEquivalenceNode<PE>*>(node_.get())->PEevalPrefix();
    }

    double guideSP() override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->guideSP();
    }

    std::vector<SolPart> getLastSolutionPart() override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->getLastSolutionPart();
    }

    std::vector<SolPart> getAllSolutionParts() override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->getAllSolutionParts();
    }

    SolPartId getSolPartId(SolPart sp) override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->getSolPartId(sp);
    }

    int getSolutionPartWeight(SolPartId id) override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->getSolutionPartWeight(id);
    }

    double getSolutionPartEvaporationRate(SolPartId id) override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->getSolutionPartEvaporationRate(id);
    }


    std::vector<SolPart> enumerateSolutionParts() override {
        return dynamic_cast<SolutionPartsNode<SolPart>*>(node_.get())->enumerateSolutionParts();
    }
};

}  // namespace cats
