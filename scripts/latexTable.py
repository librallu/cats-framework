#!/usr/bin/python3

"""
Utility script to generate latex tables.
Features:
 - [x] Add lines and columns (and refer to them by a string ID)
 - [x] Add line separator
 - [x] Add column separator
 - [ ] Add multicolheader
 - [ ] Add style for some elements
 - [ ] Get line
 - [ ] Get column
"""

class LatexTable:

    def __init__(self):
        self.lines = []
        self.linemap = {}
        self.columnmap = {}
        self.columns = []
        self.matrix = []
        self.boldmap = set()

    def addLine(self, lineid, content=[], separatorBelow=False):
        self.linemap[lineid] = len(self.lines)
        self.lines.append({"id":lineid, "sep":separatorBelow})
        self.matrix.append([e for e in content])

    def addColumn(self, colid, content=[], align="l", separatorRight=""):
        self.columnmap[colid] = len(self.columns)
        self.columns.append({"id":colid, "align":align, "sep": separatorRight})
        for i,e in enumerate(content):
            self.matrix[i].append(e)
        
    def printLatex(self, alignLineId="l", nameLineId=""):
        print("\\begin{tabular}{|"+alignLineId+"".join(["|"+e["align"]+e["sep"] for e in self.columns])+"|}")
        print("\\hline")
        # display titles
        print(nameLineId + "\t" + "".join(["& {}\t".format(e['id']) for e in self.columns]) + "\\\\")
        print("\\hline")
        for i,l in enumerate(self.lines):
            print(l['id'] + "\t" + "".join(["& {}\t".format(self.applyStyles(c, i, j)) for j,c in enumerate(self.matrix[i])]) + "\\\\")
            if l['sep']:
                print("\\hline")
        print("\\hline")
        print("\\end{tabular}")

    def registerBoldByName(self, name_line, name_col):
        if name_line not in self.linemap or name_col not in self.columnmap:
            # if error in names
            pass
        self.boldmap.add((self.linemap[name_line], self.columnmap[name_col]))

    def registerBoldById(self, i, j):
        self.boldmap.add((i, j))

    def applyStyles(self, c, i, j):
        if (i, j) in self.boldmap:
            return addBold(c)
        else:
            return c



def addDots(n):
    if n == "-":
        return n
    else:
        v = int(n)
        if v < 1000:
            return str(v)
        else:
            return addDots(v/1000)+".{:>03}".format(v%1000)

def addBold(t):
    return "\\textbf{"+t+"}"


if __name__ == "__main__":
    # initialize LatexTable object
    l = LatexTable()
    # add lines
    l.addLine("A")
    l.addLine("B", separatorBelow=True)
    l.addLine("C")
    # add columns
    l.addColumn("1", content=["A1","B1","C1"])
    l.addColumn("2", content=["A2","B2","C2"])
    # print resulting latex code
    l.printLatex()
