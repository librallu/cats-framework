#!/usr/bin/python3

import sys
import json
import matplotlib.pyplot as plt

def main(args):
    x_label = args[-1]
    titles = []
    for filename in args[1:-1]:
        with open(filename, 'r') as f:
            data = json.load(f)
            title = data['title']
            titles.append(title)
            x = [ e[x_label] for e in data['points'] ]
            y = [ e['v'] for e in data['points'] ]
            plt.plot(x, y, drawstyle='steps-post')
            plt.ylabel('objective')
            plt.xlabel(x_label)
    # plt.xscale("log")
    # plt.yscale("log")
    plt.legend(titles)
    plt.show(title)



if __name__ == "__main__":
    main(sys.argv)