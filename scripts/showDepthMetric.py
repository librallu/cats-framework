#!/usr/bin/python3

import sys
import json
import matplotlib.pyplot as plt

def main(args):
    filename = args[1]
    x_label_a = args[2]
    x_label_b = args[3]
    with open(filename, 'r') as f:
        data = json.load(f)
        y = [ e[x_label_b] for e in data[x_label_a] ]
        x = list(range(len(y)))
        print(x)
        plt.plot(x, y)
        plt.ylabel('{} ({})'.format(x_label_a, x_label_b))
        plt.xlabel("depth")
    plt.show()



if __name__ == "__main__":
    main(sys.argv)