#!/usr/bin/python3
import sys
import pydot
from graphviz import Digraph

NODEID = 0
PARENTID = 1
G = 2
H = 3
GUIDE = 4
NODEMSG = 5
ARCMSG = 6
GOAL = 7

def main(args):
    with open(args[1], 'r') as f:
        l = f.readlines()[1:]
        l = [ e.replace("\n","").split(";") for e in l ]
        g = Digraph('tree viz', filename='treeviz.gv')
        for e in l:
            if e[GOAL] == '1':
                g.node(e[NODEID], label=e[GUIDE], shape='doublecircle')
            else:
                g.node(e[NODEID], label=e[GUIDE], shape='circle')
            if e[NODEID] != e[PARENTID]:
                pare = l[int(e[PARENTID])]
                g.edge(e[PARENTID], e[NODEID], label=str(int(e[G])+int(e[H])-int(pare[G])-int(pare[H])) )
        g.view()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("USAGE: {} treeExploration.csv")
    else:
        main(sys.argv)